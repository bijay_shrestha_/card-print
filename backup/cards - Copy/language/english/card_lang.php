<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_at'] = 'Created At';
$lang['created_by'] = 'Created By';
$lang['updated_at'] = 'Updated At';
$lang['updated_by'] = 'Updated By';
$lang['deleted_at'] = 'Deleted At';
$lang['card_type'] = 'Card Type';
$lang['image'] = 'Image';

$lang['cards']='Cards';