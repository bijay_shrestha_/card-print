<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Cards
 *
 * Extends the Public_Controller class
 * 
 */

class Cards extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Cards');

        $this->load->model('cards/card_model');
        $this->lang->load('cards/card');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('cards');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'cards';
		$this->load->view($this->_container,$data);
	}
}