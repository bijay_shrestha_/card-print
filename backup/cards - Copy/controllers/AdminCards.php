<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Cards
 *
 * Extends the Project_Controller class
 * 
 */

class AdminCards extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Cards');

        $this->load->model('cards/card_model');
        $this->lang->load('cards/card');
    }

	public function index()
	{
		$data['has_upload'] = true;
		// Display Page
		$data['header'] = lang('cards');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'cards';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->card_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->card_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function combo_json()
	{
		$rows=$this->card_model->findAll();
		echo json_encode($rows);
		exit;
	}

	public function get_card($id='')
	{
		$id = $this->input->post('id');
		$result = $this->card_model->find(array('id' => $id));
		echo json_encode($result);
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->card_model->insert($data);
        }
        else
        {
            $success=$this->card_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   	private function _get_posted_data()
   	{
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		// $data['created_at'] = $this->input->post('created_at');
		// $data['created_by'] = $this->input->post('created_by');
		// $data['updated_at'] = $this->input->post('updated_at');
		// $data['updated_by'] = $this->input->post('updated_by');
		// $data['deleted_at'] = $this->input->post('deleted_at');
		$data['card_type'] = $this->input->post('card_type');
		$data['image'] = $this->input->post('image');

        return $data;
   	}

   	public function do_upload()
    {
    	// print_r($_FILES);
            $config['upload_path']          = './uploads/cards';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000000;
            $config["allowed_types"] 		= "jpeg|png|jpg|JPEG|PNG|JPG";
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;
            $config['file_name']           	= 'card_' . date('YmdHis');

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                    $error = array('error' => $this->upload->display_errors());

                    echo json_encode(array('success' => false,'msg'=>$error,'file_name'=>''));

                    // print_r($error);
                    // $this->load->view('upload_form', $error);
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());

                    echo json_encode(array('success' => true, 'msg' => false , 'file_name' => $this->upload->data('file_name')));

                    // $this->load->view('upload_success', $data);
            }
    }

    public function printCard(){
    	$id_no = $this->input->post('id_no');
    	$card_id = $this->input->post('card_id');
    	$data['id_no'] = $id_no;

    	$data['card'] = $this->card_model->find(array('id'=>$card_id));
    	
    	$this->card_model->_table = 'detail_records';
    	$data['record'] = $this->card_model->find(array('id_no'=>$id_no));
    	$date = $data['record']->date_of_birth;
    	$data['record']->date_of_birth = date('d-m-Y',strtotime($date));
		// Display Page
		$data['header'] = lang('cards');
		if(in_array($card_id, array(1,2,5,8,10,15))){
			$data['page'] = $this->config->item('template_admin') . "download";
		}elseif (in_array($card_id, array(3,6,7,11,12))) {
			$data['page'] = $this->config->item('template_admin') . "download_card_no";
		}elseif(in_array($card_id, array(9))){
			$data['page'] = $this->config->item('template_admin') . "download_card_no_top";
		}elseif(in_array($card_id,array(14))){
			$data['page'] = $this->config->item('template_admin') . "download_outing";
		}else{
			$data['page'] = $this->config->item('template_admin') . "back_of_id";
		}
		$data['module'] = 'cards';
		$this->load->view($data['page'],$data);

		$this->card_model->_table = "print_record_logs";
		$this->card_model->insert(array('id_no'=>$id_no, 'card_id'=>$card_id));
    }

    public function test()
    {
    	print_r($this->get_dpi());
    }
    public function get_dpi(){
    	$filename = "uploads/cards/card_20190214222532.jpg";

	    $a = fopen($filename,'r');
	    $string = fread($a,20);
	    fclose($a);

	    $data = bin2hex(substr($string,14,5));
	    print_r(bin2hex($string));
	    echo '<br>';
	    print_r($data);
	    $x = substr($data,0,4);
	    $y = substr($data,0,4);
	    $z = substr($data,10,14);

	    return array(hexdec($x),hexdec($y),hexdec($z));
	} 

	public function test1($value='')
	{
		//Read the jpeg image
		$path = "uploads/cards/card_20190214222532.jpg";
		$image = file_get_contents($path);
		 
		$image = substr_replace($image, pack("cnn", 1, 300, 300), 13, 5);
		 
		header("Content-type: image/jpeg");
		header('Content-Disposition: attachment; filename="'.basename($path).'"');
		 
		echo $image;
	}

	public function test2($value='')
	{
		  $filename = "uploads/cards/card_20190214222532.jpg";
 
		  // open file for writing
		  $f = fopen($filename, 'r+');
		 
		  $string = fread($f,20);
		  // show dpi before 
		  echo hexdec(bin2hex(substr($string, 14, 2))) . ' ' . hexdec(bin2hex(substr($string, 16, 2))) .'<br/>';
		 
		  // update dpi
		  $image = substr_replace($string, pack("Cnn", 0x01, 90, 90), 13, 5);
		 
		  // show dpi after
		  echo hexdec(bin2hex(substr($image, 14, 2))) . ' ' . hexdec(bin2hex(substr($image, 16, 2))) .'<br/>';
		 
		  // write and close
		  fwrite($f, $image,  filesize($filename));
		  fclose($f);
	}

	public function crop_test()
	{
		$data['header'] = lang('cards');
		$data['page'] = $this->config->item('template_admin') . "crop_test";
		$data['module'] = 'cards';
		$this->load->view($this->_container,$data);
	}

	function getJPEGresolution($filename){
		$filename = "uploads/cards/card_20190214222532.jpg";
	    $outRez=array();
	    // Read the file
	    ob_start(); // start a new output buffer
	    $image   = file_get_contents($filename);
	    
	    // grab DPI information from the JPG header
	    $outRez["xDPI"] = ord($image[15]);
	    $outRez["yDPI"] = ord($image[17]);
	    ob_end_clean(); // stop this output buffer

	    //xDPI and yDPI should equal in value... but we output both anyway...
	    print_r($outRez);
	    return($outRez);
	}//end function getJPEGresolution

	public function test3()
	{
		$image = 'uploads/cards/card_20190214222532.jpg';
		$file = NULL;
		imagejpeg($image, $file, 75); 

		  // Change DPI 
		  $dpi_x   = 150; 
		  $dpi_y   = 150; 
		  
		  // Read the file 
		  $size    = filesize($file); 
		  $image   = file_get_contents($file); 

		  // Update DPI information in the JPG header 
		  $image[13] = chr(1); 
		  $image[14] = chr(floor($dpi_x/256)); 
		  $image[15] = chr(      $dpi_x%256); 
		  $image[16] = chr(floor($dpi_y/256)); 
		  $image[17] = chr(      $dpi_y%256); 

		  // Write the new JPG 
		  $f = fopen($file, 'w'); 
		  fwrite($f, $msg, $size); 
		  fclose($f);
	}
}