
<!-- <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.js"></script> -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->

<script type="text/javascript" src="<?php echo base_url()?>assets/js/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.plugin.html2canvas.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/canvas2image.js"></script>

<style type="text/css">
    
    .container {
        position: relative;
        text-align: center;
        background-color: white;
    }
    .bottom-right {
        position: absolute;
        bottom: 8px;
        right: 16px;
    }
    h1{
        font-size: 774px;
        font-weight: 600;
        margin-bottom: -2077px;
        color: red;
        opacity: 0.4;
        margin-right: 109px;
        margin-top: 308px;
        font-family: Arial, Helvetica, sans-serif;
        font-stretch: normal;
    }
    .background{
        height: 850px;
        /*padding: 25px;*/
        /*background: url('<?php echo base_url('uploads/cards/'.$card->image)?>');*/
        background-repeat: no-repeat;
        background-size: 1354px 850px ;
    }
    .text{
        color: #000;
        position: relative;
        top:300px;
        left: 10px;
        font-size: 60px;
        text-align: center;
    }

</style>

<button type="button" class="btn btn-primary"id="btnSave">Convert</button>

<div class="container">   
    <div id="myCanvas" style="width: 1354px">
        <div class="background">
            <div class="text">
                <p>If found please return to</p>
                <p>British Embassy</p>
                <p>Kathmandu</p>
                <p>or the nearest police station</p>
                <p>Tel. No.: +977 1 4237100</p>
            </div>
        </div>
        <!-- <img src="<?php echo base_url('uploads/cards/card_20190214222532.jpg')?>" alt="card" style="width:100%;"> -->
        <div class></div>
 
        <!-- <div class="bottom-right"><h1>S</h1></div> -->
  
    </div>
</div>

<script type="text/javascript">
    
    function download() {
        var download = document.getElementById("download");
        var image = document.getElementById("myCanvas").toDataURL("image/png").replace("image/png", "image/octet-stream");
        download.setAttribute("href", image);
        //download.setAttribute("download","archive.png");
    }
</script>


 <script type="text/javascript">
    $(function() {

        $("#btnSave").click(function() {
                                  
            $(".gm-style>div:first>div").css({
                "transform":"none",
            })

            html2canvas($("#myCanvas"), {
                // useCORS: true,
                onrendered: function(canvas) {
                var dataUrl= canvas.toDataURL('image/png',1.0);
                document.write('<a href="'+dataUrl+'" download>Download</a><br><img src="' + dataUrl + '"/>');
                // saveAs(canvas.toDataURL(), 'canvas.png');
                // document.body.appendChild(canvas);
               }

            });

        });
    });
</script>