
<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/js/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.plugin.html2canvas.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/canvas2image.js"></script>

<style type="text/css">
    
    .container {
        position: relative;
        text-align: center;
        background-color: white;
    }
    .bottom-right {
        position: absolute;
        bottom: 8px;
        right: 16px;
    }
    h1{
        font-size: 774px;
        font-weight: 600;
        margin-bottom: -2077px;
        color: red;
        opacity: 0.4;
        margin-right: 109px;
        margin-top: 308px;
        font-family: Arial, Helvetica, sans-serif;
        font-stretch: normal;
    }
    .image{
        position: relative;
        top:80px;
        left: 100px;
        height: 100px;
        width: 100px;
        background: url('<?php echo base_url('uploads/detail/'.$record->image)?>');
        background-size: cover; 
        background-position: center;
        background-repeat: no-repeat;
    }
    .background{
        height: 325px;
        /*padding: 25px;*/
        background: url('<?php echo base_url('uploads/cards/'.$card->image)?>');
        background-repeat: no-repeat;
        background-size: 204px 325px;
    }
    @font-face {
        font-family: myFirstFont;
        src: url(<?php echo base_url('assets/fonts/Times_New_Romance.ttf')?>);
    }
    .text{
        color: #000;
        position: relative;
        top:100px;
        left: 10px;
        font-family: myFirstFont;
        font-size: 11px;
        text-align: left;
    }

</style>

<button type="button" class="btn btn-primary"id="btnSave">Download</button>

<div class="container">   
    <div id="myCanvas" style="width: 204px">
        <div class="background">
            <div class="image" >
                <!-- <img src="<?php echo base_url()?>"> -->
            </div>
            <div class="text">
                <b>ID No.: <?php echo $record->id_no?></b><br>
                <b><?php echo $record->name?></b><br>
                <?php echo ($record->blood_group)?"<b>Blood Grp: $record->blood_group</b><br>":""?>
                <?php echo ($record->department)?"<b>$record->department</b><br>":""?>
            </div>
        </div>
        <!-- <img src="<?php echo base_url('uploads/cards/card_20190214222532.jpg')?>" alt="card" style="width:100%;"> -->
        <div class></div>
 
        <!-- <div class="bottom-right"><h1>S</h1></div> -->
  
    </div>
</div>

<script type="text/javascript">
    
    function download() {
        var download = document.getElementById("download");
        var image = document.getElementById("myCanvas").toDataURL("image/png").replace("image/png", "image/octet-stream");
        download.setAttribute("href", image);
        //download.setAttribute("download","archive.png");
    }
</script>


 <script type="text/javascript">
    $(function() {

        $("#btnSave").click(function() {
                                  
            $(".gm-style>div:first>div").css({
                "transform":"none",
            })

            html2canvas($("#myCanvas"), {
                // useCORS: true,
                onrendered: function(canvas) {
                var dataUrl= canvas.toDataURL('image/png',1.0);
                document.write('<img src="' + dataUrl + '"/>');
                // saveAs(canvas.toDataURL(), 'canvas.png');
                // document.body.appendChild(canvas);
               }

            });

        });
    });
</script>