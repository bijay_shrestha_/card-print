<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_mst_cards
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_mst_cards extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('mst_cards'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_key(array('id','sender_id','receiver_id','date_read'));

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_at'            => array('type' => 'date',          'null'       => FALSE,   'default'  => null),
                'created_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => FALSE),
                'updated_at'            => array('type' => 'date',          'null'       => FALSE,   'default'  => null),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => FALSE),
                'deleted_at'            => array('type' => 'datetime',      'null'       => FALSE,   'default'  => null),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => TRUE),
                'card_type'             => array('type' => 'varchar',       'constraint' => 255,     'null'     => FALSE),
                'image'                 => array('type' => 'varchar',       'constraint' => 255,     'null'     => FALSE),
             ));

            $this->dbforge->create_table('mst_cards', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('mst_cards');
    }
}