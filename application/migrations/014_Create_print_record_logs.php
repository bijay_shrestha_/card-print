<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_print_record_logs
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_print_record_logs extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('print_record_logs'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_key(array('id','sender_id','receiver_id','date_read'));

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_at'            => array('type' => 'date',          'null'       => FALSE,   'default'  => null),
                'created_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => FALSE),
                'updated_at'            => array('type' => 'date',          'null'       => FALSE,   'default'  => null),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => FALSE),
                'deleted_at'            => array('type' => 'datetime',      'null'       => FALSE,   'default'  => null),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => TRUE),
                'id_no'                 => array('type' => 'varchar',       'constraint' => 255,     'null'     => FALSE),
                'card_id'		            => array('type' => 'varchar',       'constraint' => 255,     'null'     => true),
             ));

            $this->dbforge->create_table('print_record_logs', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('print_record_logs');
    }
}