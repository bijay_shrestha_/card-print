<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* PROJECT
*
* @package         PROJECT
* @author          <AUTHOR_NAME>
* @copyright       Copyright (c) 2016
*/

// ---------------------------------------------------------------------------

/**
* Migration_Create_detail_records
*
* Extends the CI_Migration class
* 
*/
class Migration_Create_detail_records extends CI_Migration {

    function up() 
    {       

        if ( ! $this->db->table_exists('detail_records'))
        {
            // Setup Keys 
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_key(array('id','sender_id','receiver_id','date_read'));

            $this->dbforge->add_field(array(
                'id'                    => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'auto_increment' => TRUE),
                'created_at'            => array('type' => 'date',          'null'       => FALSE,   'default'  => null),
                'created_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => FALSE),
                'updated_at'            => array('type' => 'date',          'null'       => FALSE,   'default'  => null),
                'updated_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => FALSE),
                'deleted_at'            => array('type' => 'datetime',      'null'       => FALSE,   'default'  => null),
                'deleted_by'            => array('type' => 'int',           'constraint' => 11,      'unsigned' => TRUE, 'null' => TRUE),
                'name'					=> array('type' => 'varchar',       'constraint' => 255,     'null'     => FALSE),
                'id_no'                 => array('type' => 'varchar',       'constraint' => 255,     'null'     => FALSE),
                'employee_number'		=> array('type' => 'varchar',       'constraint' => 255,     'null'     => true),
                'image'                 => array('type' => 'varchar',       'constraint' => 255,     'null'     => true),
                'blood_group'			=> array('type' => 'varchar',       'constraint' => 255,     'null'     => true),
                'date_of_birth'			=> array('type' => 'date',       	'null'     	 => true,	 'default'  => null),
                'department'            => array('type' => 'varchar',       'constraint' => 255,     'null'     => true),
                'record_type'			=> array('type' => 'varchar',       'constraint' => 255,     'null'     => true),
             ));

            $this->dbforge->create_table('detail_records', TRUE);
        }
    }

    function down() 
    {
        $this->dbforge->drop_table('detail_records');
    }
}