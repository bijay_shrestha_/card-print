<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * PACKAGE DESCRIPTION
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/** @defgroup module_project_controller Project Controller Module
 * \brief Master Module for Project Controller
 * \details This module is used for managing data as master records for taxable and non taxable item of different project controller
 */

/**
 * @addtogroup module_project_controller 
 * @{
 */

/**
 * \class Project Controller
 * \brief Controller Class for managing master items of different project controller 
 */

/**
 *@}
 */

class Project_Controller extends Admin_Controller {

    /**
    *
    * Constructor of Project_Contoller Controller
    *
    * @access  public
    * @param   null
    */
	public function __construct()
    {
		parent::__construct();
	}

    /**
    *
    * Search in index page
    *
    * @access  public
    * @param   null
    * @return  null
    */
	public function _get_search_param()
    {
    	//search_params helper (project_helper);
		search_params();
	}

    public function get_groups_combo_json() 
    {
        $this->load->model('groups/group_model');

        // $this->db->where_not_in('id', array(1,2));
        $current_user_groups = $this->aauth->get_user_groups();
        if(isset($current_user_groups[1])) {
            $this->db->where('id >', $current_user_groups[1]->group_id);
        }

        $this->group_model->order_by('group_name asc');
        
        $rows=$this->group_model->findAll(null, array('id','group_name'));

        array_unshift($rows, array('id' => '0', 'name' => 'Select Group'));

        echo json_encode($rows);
        exit;
    }

    public function check_duplicate() 
    {
        list($module, $model) = explode("/", $this->input->post('model'));
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        
        $this->db->where($field, $value);

        $this->load->model($this->input->post('model'));

        if ($this->input->post('id')) {
            $this->db->where('id <>', $this->input->post('id'));
        }

        $total=$this->$model->find_count();

        if ($total == 0) 
            echo json_encode(array('success' => true));
        else
            echo json_encode(array('success' => false));
    }

    public function read_file($file_name = 'userfile',$upload_path,$index,$date_index = NULL) {
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = 'xlsx|csv|xls';
        $config['max_size'] = 100000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file_name)) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        $file = FCPATH . $config['upload_path'] . '/' . $data['upload_data']['file_name']; //$_FILES['fileToUpload']['tmp_name'];
        $this->load->library('Excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');

        $objReader->setReadDataOnly(false);
        $objPHPExcel = $objReader->load($file); // error in this line
        $index = $index;
        $raw_data = array();
        $data = array();
        $view_data = array();
        foreach ($objPHPExcel->getWorksheetIterator() as $key => $worksheet) {
            if ($key == 0) {
                $worksheetTitle = $worksheet->getTitle();
                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                $nrColumns = ord($highestColumn) - 64;

                for ($row = 2; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        if(!$date_index == NULL){
                            if(in_array($index[$col], $date_index)){
                                $dateTimeObject = PHPExcel_Shared_Date::ExcelToPHPObject($cell->getValue());
                                $val = $dateTimeObject->format('Y-m-d');
                            }else{
                                $val = $cell->getValue();
                            }
                        }else{
                            $val = $cell->getValue();
                        }
                        $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
                        $raw_data[$row][$index[$col]] = $val;
                    }
                }
            }
        }

        // $this->db->from($table);
        // $vehicle = $this->db->get()->row_array();
        return $raw_data;

    }
}