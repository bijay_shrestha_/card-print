/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 100128
Source Host           : localhost:3306
Source Database       : db_card

Target Server Type    : MYSQL
Target Server Version : 100128
File Encoding         : 65001

Date: 2022-01-31 10:01:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for aauth_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_groups`;
CREATE TABLE `aauth_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_groups
-- ----------------------------
INSERT INTO `aauth_groups` VALUES ('1', 'Member', 'Member Default Access Group');
INSERT INTO `aauth_groups` VALUES ('2', 'Super Administrator', 'Super Administrator Access Group');
INSERT INTO `aauth_groups` VALUES ('100', 'Administrator', 'Administrator Access Group');
INSERT INTO `aauth_groups` VALUES ('200', 'Staff', 'Group for staff');

-- ----------------------------
-- Table structure for aauth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_group_permissions`;
CREATE TABLE `aauth_group_permissions` (
  `perm_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_group_permissions
-- ----------------------------
INSERT INTO `aauth_group_permissions` VALUES ('1', '1');
INSERT INTO `aauth_group_permissions` VALUES ('6', '100');
INSERT INTO `aauth_group_permissions` VALUES ('7', '100');

-- ----------------------------
-- Table structure for aauth_login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `aauth_login_attempts`;
CREATE TABLE `aauth_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_login_attempts
-- ----------------------------
INSERT INTO `aauth_login_attempts` VALUES ('1', '::1', '2020-11-10 17:08:41', '1');
INSERT INTO `aauth_login_attempts` VALUES ('2', '::1', '2021-11-02 16:40:04', '9');

-- ----------------------------
-- Table structure for aauth_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_permissions`;
CREATE TABLE `aauth_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_permissions
-- ----------------------------
INSERT INTO `aauth_permissions` VALUES ('1', 'Control Panel', 'Control Panel');
INSERT INTO `aauth_permissions` VALUES ('2', 'System', 'System');
INSERT INTO `aauth_permissions` VALUES ('3', 'Users', 'Users');
INSERT INTO `aauth_permissions` VALUES ('4', 'Groups', 'Groups');
INSERT INTO `aauth_permissions` VALUES ('5', 'Permissions', 'Permissions');
INSERT INTO `aauth_permissions` VALUES ('6', 'Card', 'all access for Card');
INSERT INTO `aauth_permissions` VALUES ('7', 'Records', 'all access for Records');

-- ----------------------------
-- Table structure for aauth_pms
-- ----------------------------
DROP TABLE IF EXISTS `aauth_pms`;
CREATE TABLE `aauth_pms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) unsigned NOT NULL,
  `receiver_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(11) NOT NULL DEFAULT '0',
  `pm_deleted_receiver` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_sender_id_receiver_id_date_read` (`id`,`sender_id`,`receiver_id`,`date_read`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_pms
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_sub_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_sub_groups`;
CREATE TABLE `aauth_sub_groups` (
  `group_id` int(11) unsigned NOT NULL,
  `subgroup_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_sub_groups
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_users
-- ----------------------------
DROP TABLE IF EXISTS `aauth_users`;
CREATE TABLE `aauth_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `banned` int(1) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `totp_secret` varchar(16) DEFAULT NULL,
  `ip_address` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_users
-- ----------------------------
INSERT INTO `aauth_users` VALUES ('1', 'superadmin@gmail.com', '8b235284a9f7a82364468e52dab386f33844421b481113794e0b4d634c86d0f3', 'superadmin', 'Super Administrator', '0', '2021-06-30 11:57:34', '2021-06-30 11:58:02', '2019-02-13 21:53:59', null, null, null, null, null, '::1');
INSERT INTO `aauth_users` VALUES ('2', 'admin@no.com', '52b3a93aac36bd14b3a1c9e7118f79981d14d39c6fd5118884d7544e58232a8d', 'admin', 'Administrator', '0', '2019-11-27 08:24:24', '2019-11-27 08:41:47', '2019-02-13 21:53:59', null, null, null, null, null, '::1');
INSERT INTO `aauth_users` VALUES ('1001', 'bjrukin995@gmail.com', '569df70b025029830b909a8af8b0fcf3499cb1062830580f315b3f8580217452', 'staff', 'first staff', '0', '2019-02-13 22:01:46', '2019-02-13 22:01:55', '2019-02-13 22:00:58', null, null, null, null, null, '::1');

-- ----------------------------
-- Table structure for aauth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_groups`;
CREATE TABLE `aauth_user_groups` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_groups
-- ----------------------------
INSERT INTO `aauth_user_groups` VALUES ('1', '1');
INSERT INTO `aauth_user_groups` VALUES ('1', '2');
INSERT INTO `aauth_user_groups` VALUES ('2', '1');
INSERT INTO `aauth_user_groups` VALUES ('2', '100');
INSERT INTO `aauth_user_groups` VALUES ('1001', '1');
INSERT INTO `aauth_user_groups` VALUES ('1001', '200');

-- ----------------------------
-- Table structure for aauth_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_permissions`;
CREATE TABLE `aauth_user_permissions` (
  `perm_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_user_variables
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_variables`;
CREATE TABLE `aauth_user_variables` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aauth_user_variables
-- ----------------------------

-- ----------------------------
-- Table structure for detail_records
-- ----------------------------
DROP TABLE IF EXISTS `detail_records`;
CREATE TABLE `detail_records` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `id_no` varchar(255) NOT NULL,
  `employee_number` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `blood_group` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `record_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of detail_records
-- ----------------------------
INSERT INTO `detail_records` VALUES ('2', null, '0', '2019-02-24', '1', '2019-02-27 16:21:27', '2', 'Rajeswori Shrestha', '024', '984041', 'card_20190224163535.JPG', 'O+ve', '1977-02-03', 'SECURITY', 'BE Staff');
INSERT INTO `detail_records` VALUES ('3', null, '0', '2019-02-24', '1', '2019-02-27 16:21:23', '2', 'Yogeshwor Shah', '025', '446647', 'card_20190224163521.JPG', 'A+ve', '1965-11-07', 'MANAGEMENT', 'BE Staff');
INSERT INTO `detail_records` VALUES ('4', null, '0', '2019-02-24', '1', '2019-02-27 16:21:20', '2', 'Manju Sharma', '026', '189832', 'card_20190224163510.JPG', 'A-ve', '1985-11-13', 'RALON', 'BE Staff');
INSERT INTO `detail_records` VALUES ('5', null, '0', '2019-02-24', '1', '2019-02-27 16:21:17', '2', 'Prem Rai', '027', '195750', 'card_20190224163500.jpg', 'O+ve', '1965-02-27', 'MANAGEMENT', 'BE Staff');
INSERT INTO `detail_records` VALUES ('6', null, '0', '2019-02-24', '1', '2019-02-27 16:21:13', '2', 'Padam Shrestha', '028', '658933', 'card_20190224163448.jpg', 'O+ve', '1972-05-05', 'TRANSPORT', 'BE Staff');
INSERT INTO `detail_records` VALUES ('7', null, '0', '2019-02-24', '1', '2019-02-27 16:21:10', '2', 'Arun Ale', '029', '921903', 'card_20190224163435.JPG', 'B+ve', '1982-01-20', 'MANAGEMENT', 'BE Staff');
INSERT INTO `detail_records` VALUES ('8', null, '0', null, '0', '2019-02-27 16:21:07', '2', 'Raju Maharjan', '030', '615891', null, 'O+ve', '1972-01-15', 'TRANSPORT', 'BE Staff');
INSERT INTO `detail_records` VALUES ('9', null, '0', '2019-02-27', '2', '2019-02-27 16:21:03', '2', 'Purna Lama', '031', '753796', '5c7601a5d9175.png', 'A+ve', '1984-08-22', 'SECURITY', 'BE Staff');
INSERT INTO `detail_records` VALUES ('17', '2019-02-27', '2', '2019-02-27', '2', '2019-02-27 09:38:13', '2', 'test', '1', '123', null, 'A+ve', '2015-02-02', 'test', 'Family');
INSERT INTO `detail_records` VALUES ('18', '2019-02-27', '2', '2019-02-27', '2', '2019-02-27 09:38:12', '2', 'test1', '2', '234', null, 'A+ve', '2015-02-02', 'test1', 'Family');
INSERT INTO `detail_records` VALUES ('19', '2019-02-27', '2', '2019-02-27', '2', '2019-02-27 09:38:10', '2', 'test', '1', '123', null, 'A+ve', '2015-02-02', 'test', 'Family');
INSERT INTO `detail_records` VALUES ('20', '2019-02-27', '2', '2019-02-27', '2', '2019-02-27 09:38:08', '2', 'test1', '2', '234', null, 'A+ve', '2015-02-02', 'test1', 'Family');
INSERT INTO `detail_records` VALUES ('21', '2019-02-27', '2', '2019-02-27', '2', '2019-02-27 16:20:59', '2', 'test', '02', '369', '369.png', '', null, '', 'BE Staff');
INSERT INTO `detail_records` VALUES ('22', '2019-02-27', '2', '2019-02-27', '2', '2019-02-27 16:20:55', '2', 'PUJAN J. GURUNG', '033', '140598', '140598.png', '0 +ve', '1982-11-08', 'SECURITY', 'BE Staff');
INSERT INTO `detail_records` VALUES ('23', '2019-02-27', '2', '2019-02-27', '2', '2019-03-10 17:09:59', '2', 'Sao Mo Horm', '048', '', '.png', 'O+ve', '2019-02-27', '', 'Family');
INSERT INTO `detail_records` VALUES ('24', '2019-02-27', '2', '2019-02-27', '2', null, null, 'Maj. Hebindra Pun', '227', '21167999', '21167999.png', 'O+ve', '2019-02-27', 'DEFENCE', 'BE Staff');
INSERT INTO `detail_records` VALUES ('25', '2019-02-28', '2', '2019-02-28', '2', null, null, 'Jun Kumar Gurung', '243', '', '.png', 'O+ve', '2019-02-28', 'G4S', 'Contractor');
INSERT INTO `detail_records` VALUES ('26', '2019-03-03', '2', '2019-03-03', '2', '2019-03-03 16:03:51', '2', 'Ishwor Kumar Shrestha', '244', '', '.png', '', '2019-03-03', 'G4s', 'Contractor');
INSERT INTO `detail_records` VALUES ('27', '2019-03-03', '2', '2019-03-03', '2', null, null, 'Ishwor Kumar Shrestha', '244', '', '.png', '', '1964-12-06', 'G4s', 'Contractor');
INSERT INTO `detail_records` VALUES ('28', '2019-03-07', '2', '2019-03-07', '2', null, null, 'Pujan Jung Gurung', '033', '140598', '140598.png', '0 +ve', '1982-11-08', 'Security', 'BE Staff');
INSERT INTO `detail_records` VALUES ('29', '2019-03-07', '2', '2019-03-07', '2', null, null, 'Sandesh Rai', '245', '', '.png', 'O+ve', '2019-03-07', 'G4S', 'Contractor');
INSERT INTO `detail_records` VALUES ('30', '2019-03-08', '2', '2019-03-08', '2', null, null, 'Dhurba Raj Lama', '043', '', '.png', 'O +ve', '2032-04-15', '', 'House');
INSERT INTO `detail_records` VALUES ('31', '2019-03-10', '2', '2019-03-10', '2', null, null, 'Neha Rana Magar', '148', '737745', '737745.png', 'O+ve', '1989-10-05', 'SECURITY', 'BE Staff');
INSERT INTO `detail_records` VALUES ('32', '2019-03-10', '2', '2019-03-10', '2', null, null, 'Rabi Lohala', '118', '887755', '887755.png', 'O+ve', '2019-03-10', 'Security', 'BE Staff');
INSERT INTO `detail_records` VALUES ('33', '2019-05-09', '2', '2019-05-10', '2', null, null, 'Amrita Kumal', '90', '123', '.png', 'B +ve', null, 'Smart Cleaning', 'Temporary');
INSERT INTO `detail_records` VALUES ('34', '2019-05-22', '2', '2019-05-22', '2', null, null, 'winee', '12', '001', 'nabika001.png', 'A+ve', null, 'cleaner', 'BE Staff');
INSERT INTO `detail_records` VALUES ('35', '2019-07-27', '1', '2019-07-27', '1', null, null, 'Sumit Sharma Sameer', '228', '642339', 'Sumit Sharma Sameer642339.png', 'B -ve', '1977-10-26', 'Political', 'BE Staff');

-- ----------------------------
-- Table structure for mst_cards
-- ----------------------------
DROP TABLE IF EXISTS `mst_cards`;
CREATE TABLE `mst_cards` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  `deleted_at` date DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `card_type` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `card_group` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_cards
-- ----------------------------
INSERT INTO `mst_cards` VALUES ('1', '2019-02-14', '1', '2019-02-14', '1', null, null, 'Embassy Staff', 'card_20190214222532.jpg', '1');
INSERT INTO `mst_cards` VALUES ('2', '2019-02-14', '1', '2019-02-14', '1', null, null, 'Family Pass', 'card_20190214222744.jpg', '1');
INSERT INTO `mst_cards` VALUES ('3', '2019-02-17', '1', '2019-02-17', '1', null, null, 'FCO Visitor', 'card_20190217091903.jpg', '2');
INSERT INTO `mst_cards` VALUES ('4', '2019-02-17', '1', '2019-02-17', '1', '2019-02-19', '1', 'no', 'card_20190217174119.png', null);
INSERT INTO `mst_cards` VALUES ('5', '2019-02-19', '1', '2019-02-19', '1', null, null, 'Contractor Pass', 'card_20190219112439.jpg', '1');
INSERT INTO `mst_cards` VALUES ('6', '2019-02-19', '1', '2019-02-19', '1', null, null, 'DFID Visitor', 'card_20190219112533.jpg', '2');
INSERT INTO `mst_cards` VALUES ('7', '2019-02-19', '1', '2019-02-19', '1', null, null, 'Driver Pass', 'card_20190219112637.jpg', '2');
INSERT INTO `mst_cards` VALUES ('8', '2019-02-19', '1', '2019-02-19', '1', null, null, 'Temporary Pass', 'card_20190219112725.jpg', '1');
INSERT INTO `mst_cards` VALUES ('9', '2019-02-19', '1', '2019-02-19', '1', null, null, 'Residence Pass', 'card_20190219112815.jpg', '3');
INSERT INTO `mst_cards` VALUES ('10', '2019-02-19', '1', '2019-02-19', '1', null, null, 'House Pass', 'card_20190219113036.jpg', '1');
INSERT INTO `mst_cards` VALUES ('11', '2019-02-19', '1', '2019-02-19', '1', null, null, 'Visitors Pass', 'card_20190219113125.jpg', '2');
INSERT INTO `mst_cards` VALUES ('12', '2019-02-19', '1', '2019-02-19', '1', null, null, 'Staff', 'card_20190219113255.jpg', '2');
INSERT INTO `mst_cards` VALUES ('13', null, '1', null, '1', null, null, 'Back of Id', '', '5');
INSERT INTO `mst_cards` VALUES ('14', '2019-02-24', '1', '2019-02-24', '1', null, null, 'Out Card', 'card_20190224153648.jpg', '4');
INSERT INTO `mst_cards` VALUES ('15', '2019-02-24', '1', '2019-02-24', '1', null, null, 'FCO Visitor with Photo', 'card_20190224160340.jpg', '1');
INSERT INTO `mst_cards` VALUES ('16', '2019-11-19', '1', '2019-11-19', '1', null, null, 'British Embassy', 'card_20191119210046.jpg', '2');
INSERT INTO `mst_cards` VALUES ('17', '2019-11-27', '2', '2019-11-27', '2', null, null, 'test', 'card_20191127083816.jpg', null);
INSERT INTO `mst_cards` VALUES ('18', '2019-11-27', '2', '2019-11-27', '2', null, null, 'test1', 'card_20191127083816.jpg', '1');

-- ----------------------------
-- Table structure for print_record_logs
-- ----------------------------
DROP TABLE IF EXISTS `print_record_logs`;
CREATE TABLE `print_record_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) unsigned NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  `id_no` varchar(255) NOT NULL,
  `card_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=298 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of print_record_logs
-- ----------------------------
INSERT INTO `print_record_logs` VALUES ('1', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('2', '2019-02-18', '1', '2019-02-18', '1', null, null, '25', '1');
INSERT INTO `print_record_logs` VALUES ('3', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('4', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '2');
INSERT INTO `print_record_logs` VALUES ('5', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '2');
INSERT INTO `print_record_logs` VALUES ('6', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '2');
INSERT INTO `print_record_logs` VALUES ('7', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('8', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('9', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('10', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('11', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('12', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('13', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('14', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('15', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('16', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('17', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('18', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('19', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('20', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('21', '2019-02-18', '1', '2019-02-18', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('22', '2019-02-19', '2', '2019-02-19', '2', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('23', '2019-02-19', '2', '2019-02-19', '2', null, null, '1', '');
INSERT INTO `print_record_logs` VALUES ('24', '2019-02-19', '2', '2019-02-19', '2', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('25', '2019-02-19', '2', '2019-02-19', '2', null, null, '02', '1');
INSERT INTO `print_record_logs` VALUES ('26', '2019-02-19', '2', '2019-02-19', '2', null, null, '1', '2');
INSERT INTO `print_record_logs` VALUES ('27', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '11');
INSERT INTO `print_record_logs` VALUES ('28', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '3');
INSERT INTO `print_record_logs` VALUES ('29', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('30', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('31', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('32', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('33', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('34', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('35', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('36', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('37', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('38', '2019-02-19', '1', '2019-02-19', '1', null, null, '02', '13');
INSERT INTO `print_record_logs` VALUES ('39', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('40', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('41', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('42', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('43', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('44', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('45', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('46', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('47', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('48', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('49', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('50', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('51', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('52', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('53', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('54', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('55', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('56', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('57', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('58', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('59', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('60', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('61', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('62', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('63', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('64', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('65', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('66', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('67', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('68', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('69', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('70', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('71', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('72', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('73', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('74', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('75', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('76', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('77', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('78', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('79', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('80', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('81', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('82', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('83', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('84', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('85', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('86', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '6');
INSERT INTO `print_record_logs` VALUES ('87', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '7');
INSERT INTO `print_record_logs` VALUES ('88', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('89', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '12');
INSERT INTO `print_record_logs` VALUES ('90', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('91', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('92', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('93', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('94', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('95', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('96', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('97', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('98', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('99', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('100', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('101', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('102', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('103', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('104', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('105', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('106', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('107', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('108', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('109', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('110', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('111', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '9');
INSERT INTO `print_record_logs` VALUES ('112', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '11');
INSERT INTO `print_record_logs` VALUES ('113', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('114', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('115', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('116', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('117', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('118', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('119', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('120', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('121', '2019-02-19', '1', '2019-02-19', '1', null, null, '001', '13');
INSERT INTO `print_record_logs` VALUES ('122', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('123', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '1');
INSERT INTO `print_record_logs` VALUES ('124', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('125', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('126', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '9');
INSERT INTO `print_record_logs` VALUES ('127', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '9');
INSERT INTO `print_record_logs` VALUES ('128', '2019-02-19', '1', '2019-02-19', '1', null, null, '', '13');
INSERT INTO `print_record_logs` VALUES ('129', '2019-02-19', '1', '2019-02-19', '1', null, null, '', '13');
INSERT INTO `print_record_logs` VALUES ('130', '2019-02-19', '1', '2019-02-19', '1', null, null, '', '13');
INSERT INTO `print_record_logs` VALUES ('131', '2019-02-19', '1', '2019-02-19', '1', null, null, '', '13');
INSERT INTO `print_record_logs` VALUES ('132', '2019-02-19', '1', '2019-02-19', '1', null, null, '1', '3');
INSERT INTO `print_record_logs` VALUES ('133', '2019-02-19', '2', '2019-02-19', '2', null, null, '03', '1');
INSERT INTO `print_record_logs` VALUES ('134', '2019-02-19', '2', '2019-02-19', '2', null, null, '03', '1');
INSERT INTO `print_record_logs` VALUES ('135', '2019-02-24', '2', '2019-02-24', '2', null, null, '03', '1');
INSERT INTO `print_record_logs` VALUES ('136', '2019-02-24', '2', '2019-02-24', '2', null, null, '03', '1');
INSERT INTO `print_record_logs` VALUES ('137', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '14');
INSERT INTO `print_record_logs` VALUES ('138', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '14');
INSERT INTO `print_record_logs` VALUES ('139', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '14');
INSERT INTO `print_record_logs` VALUES ('140', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '14');
INSERT INTO `print_record_logs` VALUES ('141', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '14');
INSERT INTO `print_record_logs` VALUES ('142', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '15');
INSERT INTO `print_record_logs` VALUES ('143', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('144', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('145', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('146', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('147', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('148', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('149', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('150', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('151', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('152', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('153', '2019-02-24', '1', '2019-02-24', '1', null, null, '03', '13');
INSERT INTO `print_record_logs` VALUES ('154', '2019-02-24', '1', '2019-02-24', '1', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('155', '2019-02-24', '1', '2019-02-24', '1', null, null, '031', '1');
INSERT INTO `print_record_logs` VALUES ('156', '2019-02-24', '1', '2019-02-24', '1', null, null, '009', '3');
INSERT INTO `print_record_logs` VALUES ('157', '2019-02-24', '1', '2019-02-24', '1', null, null, '009', '3');
INSERT INTO `print_record_logs` VALUES ('158', '2019-02-24', '1', '2019-02-24', '1', null, null, '009', '3');
INSERT INTO `print_record_logs` VALUES ('159', '2019-02-24', '1', '2019-02-24', '1', null, null, '243', '5');
INSERT INTO `print_record_logs` VALUES ('160', '2019-02-24', '2', '2019-02-24', '2', null, null, '244', '5');
INSERT INTO `print_record_logs` VALUES ('161', '2019-02-24', '2', '2019-02-24', '2', null, null, '244', '5');
INSERT INTO `print_record_logs` VALUES ('162', '2019-02-24', '2', '2019-02-24', '2', null, null, '024', '1');
INSERT INTO `print_record_logs` VALUES ('163', '2019-02-24', '2', '2019-02-24', '2', null, null, '50', '1');
INSERT INTO `print_record_logs` VALUES ('164', '2019-02-24', '2', '2019-02-24', '2', null, null, '50', '1');
INSERT INTO `print_record_logs` VALUES ('165', '2019-02-24', '2', '2019-02-24', '2', null, null, '12', '3');
INSERT INTO `print_record_logs` VALUES ('166', '2019-02-24', '2', '2019-02-24', '2', null, null, '10', '1');
INSERT INTO `print_record_logs` VALUES ('167', '2019-02-24', '2', '2019-02-24', '2', null, null, '', '5');
INSERT INTO `print_record_logs` VALUES ('168', '2019-02-24', '2', '2019-02-24', '2', null, null, '04', '3');
INSERT INTO `print_record_logs` VALUES ('169', '2019-02-27', '2', '2019-02-27', '2', null, null, '10', '');
INSERT INTO `print_record_logs` VALUES ('170', '2019-02-27', '2', '2019-02-27', '2', null, null, '10', '1');
INSERT INTO `print_record_logs` VALUES ('171', '2019-02-27', '2', '2019-02-27', '2', null, null, '10', '1');
INSERT INTO `print_record_logs` VALUES ('172', '2019-02-27', '2', '2019-02-27', '2', null, null, '031', '');
INSERT INTO `print_record_logs` VALUES ('173', '2019-02-27', '2', '2019-02-27', '2', null, null, '031', '1');
INSERT INTO `print_record_logs` VALUES ('174', '2019-02-27', '2', '2019-02-27', '2', null, null, '031', '1');
INSERT INTO `print_record_logs` VALUES ('175', '2019-02-27', '2', '2019-02-27', '2', null, null, '031', '');
INSERT INTO `print_record_logs` VALUES ('176', '2019-02-27', '2', '2019-02-27', '2', null, null, '031', '1');
INSERT INTO `print_record_logs` VALUES ('177', '2019-02-27', '2', '2019-02-27', '2', null, null, '02', '');
INSERT INTO `print_record_logs` VALUES ('178', '2019-02-27', '2', '2019-02-27', '2', null, null, '02', '1');
INSERT INTO `print_record_logs` VALUES ('179', '2019-02-27', '2', '2019-02-27', '2', null, null, '02', '');
INSERT INTO `print_record_logs` VALUES ('180', '2019-02-27', '2', '2019-02-27', '2', null, null, '02', '1');
INSERT INTO `print_record_logs` VALUES ('181', '2019-02-27', '2', '2019-02-27', '2', null, null, '031', '1');
INSERT INTO `print_record_logs` VALUES ('182', '2019-02-27', '2', '2019-02-27', '2', null, null, '02', '1');
INSERT INTO `print_record_logs` VALUES ('183', '2019-02-27', '2', '2019-02-27', '2', null, null, '02', '1');
INSERT INTO `print_record_logs` VALUES ('184', '2019-02-27', '2', '2019-02-27', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('185', '2019-02-27', '2', '2019-02-27', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('186', '2019-02-27', '2', '2019-02-27', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('187', '2019-02-27', '2', '2019-02-27', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('188', '2019-02-27', '2', '2019-02-27', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('189', '2019-02-27', '2', '2019-02-27', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('190', '2019-02-27', '2', '2019-02-27', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('191', '2019-02-27', '2', '2019-02-27', '2', null, null, '048', '2');
INSERT INTO `print_record_logs` VALUES ('192', '2019-02-27', '2', '2019-02-27', '2', null, null, '227', '1');
INSERT INTO `print_record_logs` VALUES ('193', '2019-02-27', '2', '2019-02-27', '2', null, null, '227', '1');
INSERT INTO `print_record_logs` VALUES ('194', '2019-02-27', '2', '2019-02-27', '2', null, null, '227', '1');
INSERT INTO `print_record_logs` VALUES ('195', '2019-02-28', '2', '2019-02-28', '2', null, null, '243', '5');
INSERT INTO `print_record_logs` VALUES ('196', '2019-03-03', '2', '2019-03-03', '2', null, null, '244', '5');
INSERT INTO `print_record_logs` VALUES ('197', '2019-03-03', '2', '2019-03-03', '2', null, null, '244', '5');
INSERT INTO `print_record_logs` VALUES ('198', '2019-03-03', '2', '2019-03-03', '2', null, null, '244', '5');
INSERT INTO `print_record_logs` VALUES ('199', '2019-03-07', '2', '2019-03-07', '2', null, null, '', '1');
INSERT INTO `print_record_logs` VALUES ('200', '2019-03-07', '2', '2019-03-07', '2', null, null, '140598', '');
INSERT INTO `print_record_logs` VALUES ('201', '2019-03-07', '2', '2019-03-07', '2', null, null, '', '1');
INSERT INTO `print_record_logs` VALUES ('202', '2019-03-07', '2', '2019-03-07', '2', null, null, '', '');
INSERT INTO `print_record_logs` VALUES ('203', '2019-03-07', '2', '2019-03-07', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('204', '2019-03-07', '2', '2019-03-07', '2', null, null, '033', '');
INSERT INTO `print_record_logs` VALUES ('205', '2019-03-07', '2', '2019-03-07', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('206', '2019-03-07', '2', '2019-03-07', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('207', '2019-03-07', '2', '2019-03-07', '2', null, null, '245', '5');
INSERT INTO `print_record_logs` VALUES ('208', '2019-03-08', '2', '2019-03-08', '2', null, null, '0473', '10');
INSERT INTO `print_record_logs` VALUES ('209', '2019-03-08', '2', '2019-03-08', '2', null, null, '043', '');
INSERT INTO `print_record_logs` VALUES ('210', '2019-03-08', '2', '2019-03-08', '2', null, null, '043', '10');
INSERT INTO `print_record_logs` VALUES ('211', '2019-03-10', '2', '2019-03-10', '2', null, null, '148', '1');
INSERT INTO `print_record_logs` VALUES ('212', '2019-03-10', '2', '2019-03-10', '2', null, null, '148', '1');
INSERT INTO `print_record_logs` VALUES ('213', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('214', '2019-03-10', '2', '2019-03-10', '2', null, null, '001', '3');
INSERT INTO `print_record_logs` VALUES ('215', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('216', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '');
INSERT INTO `print_record_logs` VALUES ('217', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('218', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('219', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('220', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('221', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('222', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('223', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('224', '2019-03-10', '2', '2019-03-10', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('225', '2019-03-11', '2', '2019-03-11', '2', null, null, '227', '1');
INSERT INTO `print_record_logs` VALUES ('226', '2019-03-11', '2', '2019-03-11', '2', null, null, '118', '2');
INSERT INTO `print_record_logs` VALUES ('227', '2019-03-14', '1', '2019-03-14', '1', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('228', '2019-05-09', '2', '2019-05-09', '2', null, null, '90', '');
INSERT INTO `print_record_logs` VALUES ('229', '2019-05-09', '2', '2019-05-09', '2', null, null, '90', '8');
INSERT INTO `print_record_logs` VALUES ('230', '2019-05-16', '2', '2019-05-16', '2', null, null, '033', '1');
INSERT INTO `print_record_logs` VALUES ('231', '2019-05-16', '2', '2019-05-16', '2', null, null, '033', '13');
INSERT INTO `print_record_logs` VALUES ('232', '2019-05-16', '2', '2019-05-16', '2', null, null, '033', '13');
INSERT INTO `print_record_logs` VALUES ('233', '2019-05-22', '2', '2019-05-22', '2', null, null, '', '13');
INSERT INTO `print_record_logs` VALUES ('234', '2019-05-22', '2', '2019-05-22', '2', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('235', '2019-05-22', '2', '2019-05-22', '2', null, null, '12', '1');
INSERT INTO `print_record_logs` VALUES ('236', '2019-05-22', '2', '2019-05-22', '2', null, null, '12', '1');
INSERT INTO `print_record_logs` VALUES ('237', '2019-07-21', '2', '2019-07-21', '2', null, null, '12', '2');
INSERT INTO `print_record_logs` VALUES ('238', '2019-07-21', '2', '2019-07-21', '2', null, null, '12', '2');
INSERT INTO `print_record_logs` VALUES ('239', '2019-07-27', '1', '2019-07-27', '1', null, null, '642339', '1');
INSERT INTO `print_record_logs` VALUES ('240', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('241', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('242', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('243', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('244', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('245', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('246', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('247', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('248', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('249', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('250', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('251', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('252', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('253', '2019-07-27', '1', '2019-07-27', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('254', '2019-08-21', '1', '2019-08-21', '1', null, null, '', '9');
INSERT INTO `print_record_logs` VALUES ('255', '2019-08-21', '1', '2019-08-21', '1', null, null, '', '9');
INSERT INTO `print_record_logs` VALUES ('256', '2019-08-21', '1', '2019-08-21', '1', null, null, '', '9');
INSERT INTO `print_record_logs` VALUES ('257', '2019-08-21', '1', '2019-08-21', '1', null, null, '', '9');
INSERT INTO `print_record_logs` VALUES ('258', '2019-08-21', '1', '2019-08-21', '1', null, null, '', '9');
INSERT INTO `print_record_logs` VALUES ('259', '2019-08-21', '1', '2019-08-21', '1', null, null, '', '9');
INSERT INTO `print_record_logs` VALUES ('260', '2019-08-21', '1', '2019-08-21', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('261', '2019-08-21', '1', '2019-08-21', '1', null, null, '228', '');
INSERT INTO `print_record_logs` VALUES ('262', '2019-08-21', '1', '2019-08-21', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('263', '2019-08-21', '1', '2019-08-21', '1', null, null, '', '9');
INSERT INTO `print_record_logs` VALUES ('264', '2019-08-21', '1', '2019-08-21', '1', null, null, '', '9');
INSERT INTO `print_record_logs` VALUES ('265', '2019-10-23', '1', '2019-10-23', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('266', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('267', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('268', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('269', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '');
INSERT INTO `print_record_logs` VALUES ('270', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('271', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('272', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('273', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('274', '2019-10-24', '1', '2019-10-24', '1', null, null, '228', '14');
INSERT INTO `print_record_logs` VALUES ('275', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '16');
INSERT INTO `print_record_logs` VALUES ('276', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '16');
INSERT INTO `print_record_logs` VALUES ('277', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '16');
INSERT INTO `print_record_logs` VALUES ('278', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '16');
INSERT INTO `print_record_logs` VALUES ('279', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '16');
INSERT INTO `print_record_logs` VALUES ('280', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '11');
INSERT INTO `print_record_logs` VALUES ('281', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '16');
INSERT INTO `print_record_logs` VALUES ('282', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '1');
INSERT INTO `print_record_logs` VALUES ('283', '2019-11-19', '1', '2019-11-19', '1', null, null, '', '2');
INSERT INTO `print_record_logs` VALUES ('284', '2019-11-27', '2', '2019-11-27', '2', null, null, '123', '16');
INSERT INTO `print_record_logs` VALUES ('285', '2020-12-11', '1', '2020-12-11', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('286', '2020-12-11', '1', '2020-12-11', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('287', '2021-04-07', '1', '2021-04-07', '1', null, null, '12', '1');
INSERT INTO `print_record_logs` VALUES ('288', '2021-04-07', '1', '2021-04-07', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('289', '2021-04-07', '1', '2021-04-07', '1', null, null, '228', '16');
INSERT INTO `print_record_logs` VALUES ('290', '2021-04-07', '1', '2021-04-07', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('291', '2021-04-07', '1', '2021-04-07', '1', null, null, '118', '5');
INSERT INTO `print_record_logs` VALUES ('292', '2021-04-07', '1', '2021-04-07', '1', null, null, '118', '1');
INSERT INTO `print_record_logs` VALUES ('293', '2021-04-07', '1', '2021-04-07', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('294', '2021-04-07', '1', '2021-04-07', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('295', '2021-04-07', '1', '2021-04-07', '1', null, null, '228', '1');
INSERT INTO `print_record_logs` VALUES ('296', '2021-06-30', '1', '2021-06-30', '1', null, null, '228', '');
INSERT INTO `print_record_logs` VALUES ('297', '2021-06-30', '1', '2021-06-30', '1', null, null, '228', '1');

-- ----------------------------
-- Table structure for project_activity_logs
-- ----------------------------
DROP TABLE IF EXISTS `project_activity_logs`;
CREATE TABLE `project_activity_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `table_pk` int(11) unsigned NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_dttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_activity_logs
-- ----------------------------
INSERT INTO `project_activity_logs` VALUES ('1', '1', 'mst_cards', '1', 'insert', '2019-02-14 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('2', '1', 'mst_cards', '2', 'insert', '2019-02-14 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('3', '1', 'mst_cards', '3', 'insert', '2019-02-17 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('4', '1', 'detail_records', '1', 'insert', '2019-02-17 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('5', '1', 'mst_cards', '4', 'insert', '2019-02-17 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('6', '1', 'detail_records', '2', 'insert', '2019-02-17 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('7', '1', 'print_record_logs', '1', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('8', '1', 'print_record_logs', '2', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('9', '1', 'print_record_logs', '3', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('10', '1', 'print_record_logs', '4', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('11', '1', 'print_record_logs', '5', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('12', '1', 'print_record_logs', '6', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('13', '1', 'print_record_logs', '7', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('14', '1', 'print_record_logs', '8', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('15', '1', 'print_record_logs', '9', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('16', '1', 'print_record_logs', '10', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('17', '1', 'print_record_logs', '11', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('18', '1', 'print_record_logs', '12', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('19', '1', 'print_record_logs', '13', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('20', '1', 'print_record_logs', '14', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('21', '1', 'print_record_logs', '15', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('22', '1', 'print_record_logs', '16', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('23', '1', 'print_record_logs', '17', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('24', '1', 'print_record_logs', '18', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('25', '1', 'print_record_logs', '19', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('26', '1', 'print_record_logs', '20', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('27', '1', 'print_record_logs', '21', 'insert', '2019-02-18 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('28', '2', 'print_record_logs', '22', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('29', '2', 'print_record_logs', '23', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('30', '2', 'print_record_logs', '24', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('31', '2', 'print_record_logs', '25', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('32', '2', 'print_record_logs', '26', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('33', '1', 'mst_cards', '5', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('34', '1', 'mst_cards', '6', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('35', '1', 'mst_cards', '7', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('36', '1', 'mst_cards', '8', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('37', '1', 'mst_cards', '9', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('38', '1', 'mst_cards', '10', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('39', '1', 'mst_cards', '11', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('40', '1', 'mst_cards', '12', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('41', '1', 'print_record_logs', '27', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('42', '1', 'print_record_logs', '28', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('43', '1', 'print_record_logs', '29', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('44', '1', 'print_record_logs', '30', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('45', '1', 'print_record_logs', '31', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('46', '1', 'print_record_logs', '32', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('47', '1', 'print_record_logs', '33', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('48', '1', 'print_record_logs', '34', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('49', '1', 'print_record_logs', '35', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('50', '1', 'print_record_logs', '36', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('51', '1', 'print_record_logs', '37', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('52', '1', 'print_record_logs', '38', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('53', '1', 'print_record_logs', '39', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('54', '1', 'print_record_logs', '40', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('55', '1', 'print_record_logs', '41', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('56', '1', 'print_record_logs', '42', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('57', '1', 'print_record_logs', '43', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('58', '1', 'print_record_logs', '44', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('59', '1', 'print_record_logs', '45', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('60', '1', 'print_record_logs', '46', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('61', '1', 'print_record_logs', '47', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('62', '1', 'print_record_logs', '48', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('63', '1', 'print_record_logs', '49', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('64', '1', 'print_record_logs', '50', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('65', '1', 'print_record_logs', '51', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('66', '1', 'print_record_logs', '52', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('67', '1', 'print_record_logs', '53', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('68', '1', 'print_record_logs', '54', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('69', '1', 'print_record_logs', '55', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('70', '1', 'print_record_logs', '56', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('71', '1', 'print_record_logs', '57', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('72', '1', 'print_record_logs', '58', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('73', '1', 'print_record_logs', '59', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('74', '1', 'print_record_logs', '60', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('75', '1', 'print_record_logs', '61', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('76', '1', 'print_record_logs', '62', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('77', '1', 'print_record_logs', '63', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('78', '1', 'print_record_logs', '64', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('79', '1', 'print_record_logs', '65', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('80', '1', 'print_record_logs', '66', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('81', '1', 'print_record_logs', '67', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('82', '1', 'print_record_logs', '68', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('83', '1', 'print_record_logs', '69', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('84', '1', 'print_record_logs', '70', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('85', '1', 'print_record_logs', '71', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('86', '1', 'print_record_logs', '72', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('87', '1', 'print_record_logs', '73', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('88', '1', 'print_record_logs', '74', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('89', '1', 'print_record_logs', '75', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('90', '1', 'print_record_logs', '76', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('91', '1', 'print_record_logs', '77', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('92', '1', 'print_record_logs', '78', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('93', '1', 'print_record_logs', '79', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('94', '1', 'print_record_logs', '80', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('95', '1', 'print_record_logs', '81', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('96', '1', 'print_record_logs', '82', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('97', '1', 'print_record_logs', '83', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('98', '1', 'print_record_logs', '84', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('99', '1', 'print_record_logs', '85', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('100', '1', 'print_record_logs', '86', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('101', '1', 'print_record_logs', '87', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('102', '1', 'print_record_logs', '88', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('103', '1', 'print_record_logs', '89', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('104', '1', 'print_record_logs', '90', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('105', '1', 'print_record_logs', '91', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('106', '1', 'print_record_logs', '92', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('107', '1', 'print_record_logs', '93', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('108', '1', 'print_record_logs', '94', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('109', '1', 'print_record_logs', '95', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('110', '1', 'print_record_logs', '96', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('111', '1', 'print_record_logs', '97', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('112', '1', 'print_record_logs', '98', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('113', '1', 'print_record_logs', '99', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('114', '1', 'print_record_logs', '100', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('115', '1', 'print_record_logs', '101', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('116', '1', 'print_record_logs', '102', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('117', '1', 'print_record_logs', '103', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('118', '1', 'print_record_logs', '104', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('119', '1', 'print_record_logs', '105', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('120', '1', 'print_record_logs', '106', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('121', '1', 'print_record_logs', '107', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('122', '1', 'print_record_logs', '108', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('123', '1', 'print_record_logs', '109', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('124', '1', 'print_record_logs', '110', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('125', '1', 'print_record_logs', '111', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('126', '1', 'print_record_logs', '112', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('127', '1', 'print_record_logs', '113', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('128', '1', 'print_record_logs', '114', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('129', '1', 'print_record_logs', '115', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('130', '1', 'print_record_logs', '116', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('131', '1', 'print_record_logs', '117', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('132', '1', 'print_record_logs', '118', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('133', '1', 'print_record_logs', '119', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('134', '1', 'print_record_logs', '120', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('135', '1', 'print_record_logs', '121', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('136', '1', 'print_record_logs', '122', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('137', '1', 'print_record_logs', '123', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('138', '1', 'print_record_logs', '124', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('139', '1', 'print_record_logs', '125', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('140', '1', 'print_record_logs', '126', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('141', '1', 'print_record_logs', '127', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('142', '1', 'print_record_logs', '128', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('143', '1', 'print_record_logs', '129', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('144', '1', 'print_record_logs', '130', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('145', '1', 'print_record_logs', '131', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('146', '1', 'print_record_logs', '132', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('147', '2', 'detail_records', '3', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('148', '2', 'print_record_logs', '133', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('149', '2', 'detail_records', '3', 'update', '2019-02-19 15:32:44');
INSERT INTO `project_activity_logs` VALUES ('150', '2', 'print_record_logs', '134', 'insert', '2019-02-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('151', '2', 'print_record_logs', '135', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('152', '2', 'print_record_logs', '136', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('153', '1', 'mst_cards', '14', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('154', '1', 'print_record_logs', '137', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('155', '1', 'print_record_logs', '138', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('156', '1', 'print_record_logs', '139', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('157', '1', 'detail_records', '3', 'update', '2019-02-24 15:55:13');
INSERT INTO `project_activity_logs` VALUES ('158', '1', 'print_record_logs', '140', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('159', '1', 'print_record_logs', '141', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('160', '1', 'mst_cards', '15', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('161', '1', 'print_record_logs', '142', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('162', '1', 'print_record_logs', '143', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('163', '1', 'print_record_logs', '144', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('164', '1', 'print_record_logs', '145', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('165', '1', 'print_record_logs', '146', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('166', '1', 'print_record_logs', '147', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('167', '1', 'print_record_logs', '148', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('168', '1', 'print_record_logs', '149', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('169', '1', 'print_record_logs', '150', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('170', '1', 'print_record_logs', '151', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('171', '1', 'print_record_logs', '152', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('172', '1', 'print_record_logs', '153', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('173', '1', 'detail_records', '11', 'update', '2019-02-24 16:33:52');
INSERT INTO `project_activity_logs` VALUES ('174', '1', 'detail_records', '10', 'update', '2019-02-24 16:34:10');
INSERT INTO `project_activity_logs` VALUES ('175', '1', 'detail_records', '9', 'update', '2019-02-24 16:34:27');
INSERT INTO `project_activity_logs` VALUES ('176', '1', 'detail_records', '7', 'update', '2019-02-24 16:34:36');
INSERT INTO `project_activity_logs` VALUES ('177', '1', 'detail_records', '6', 'update', '2019-02-24 16:34:49');
INSERT INTO `project_activity_logs` VALUES ('178', '1', 'detail_records', '5', 'update', '2019-02-24 16:35:02');
INSERT INTO `project_activity_logs` VALUES ('179', '1', 'detail_records', '4', 'update', '2019-02-24 16:35:12');
INSERT INTO `project_activity_logs` VALUES ('180', '1', 'detail_records', '3', 'update', '2019-02-24 16:35:23');
INSERT INTO `project_activity_logs` VALUES ('181', '1', 'detail_records', '2', 'update', '2019-02-24 16:35:36');
INSERT INTO `project_activity_logs` VALUES ('182', '1', 'print_record_logs', '154', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('183', '1', 'print_record_logs', '155', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('184', '1', 'print_record_logs', '156', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('185', '1', 'print_record_logs', '157', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('186', '1', 'print_record_logs', '158', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('187', '1', 'detail_records', '12', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('188', '1', 'print_record_logs', '159', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('189', '2', 'detail_records', '13', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('190', '2', 'print_record_logs', '160', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('191', '2', 'print_record_logs', '161', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('192', '2', 'detail_records', '14', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('193', '2', 'print_record_logs', '162', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('194', '2', 'detail_records', '15', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('195', '2', 'print_record_logs', '163', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('196', '2', 'detail_records', '15', 'update', '2019-02-24 18:37:48');
INSERT INTO `project_activity_logs` VALUES ('197', '2', 'print_record_logs', '164', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('198', '2', 'print_record_logs', '165', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('199', '2', 'detail_records', '16', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('200', '2', 'print_record_logs', '166', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('201', '2', 'print_record_logs', '167', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('202', '2', 'print_record_logs', '168', 'insert', '2019-02-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('203', '2', 'print_record_logs', '169', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('204', '2', 'print_record_logs', '170', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('205', '2', 'print_record_logs', '171', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('206', '2', 'detail_records', '9', 'update', '2019-02-27 09:04:04');
INSERT INTO `project_activity_logs` VALUES ('207', '2', 'print_record_logs', '172', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('208', '2', 'print_record_logs', '173', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('209', '2', 'print_record_logs', '174', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('210', '2', 'print_record_logs', '175', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('211', '2', 'print_record_logs', '176', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('212', '2', 'detail_records', '17', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('213', '2', 'detail_records', '18', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('214', '2', 'detail_records', '19', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('215', '2', 'detail_records', '20', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('216', '2', 'detail_records', '21', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('217', '2', 'detail_records', '21', 'update', '2019-02-27 09:49:45');
INSERT INTO `project_activity_logs` VALUES ('218', '2', 'print_record_logs', '177', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('219', '2', 'print_record_logs', '178', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('220', '2', 'print_record_logs', '179', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('221', '2', 'print_record_logs', '180', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('222', '2', 'print_record_logs', '181', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('223', '2', 'detail_records', '21', 'update', '2019-02-27 09:53:33');
INSERT INTO `project_activity_logs` VALUES ('224', '2', 'print_record_logs', '182', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('225', '2', 'print_record_logs', '183', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('226', '2', 'detail_records', '22', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('227', '2', 'print_record_logs', '184', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('228', '2', 'print_record_logs', '185', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('229', '2', 'detail_records', '22', 'update', '2019-02-27 10:19:05');
INSERT INTO `project_activity_logs` VALUES ('230', '2', 'print_record_logs', '186', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('231', '2', 'print_record_logs', '187', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('232', '2', 'print_record_logs', '188', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('233', '2', 'print_record_logs', '189', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('234', '2', 'print_record_logs', '190', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('235', '2', 'detail_records', '23', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('236', '2', 'print_record_logs', '191', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('237', '2', 'detail_records', '24', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('238', '2', 'print_record_logs', '192', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('239', '2', 'detail_records', '24', 'update', '2019-02-27 16:09:55');
INSERT INTO `project_activity_logs` VALUES ('240', '2', 'print_record_logs', '193', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('241', '2', 'print_record_logs', '194', 'insert', '2019-02-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('242', '2', 'detail_records', '25', 'insert', '2019-02-28 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('243', '2', 'print_record_logs', '195', 'insert', '2019-02-28 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('244', '2', 'detail_records', '26', 'insert', '2019-03-03 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('245', '2', 'detail_records', '27', 'insert', '2019-03-03 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('246', '2', 'detail_records', '27', 'update', '2019-03-03 16:06:55');
INSERT INTO `project_activity_logs` VALUES ('247', '2', 'print_record_logs', '196', 'insert', '2019-03-03 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('248', '2', 'print_record_logs', '197', 'insert', '2019-03-03 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('249', '2', 'print_record_logs', '198', 'insert', '2019-03-03 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('250', '2', 'detail_records', '28', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('251', '2', 'print_record_logs', '199', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('252', '2', 'print_record_logs', '200', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('253', '2', 'print_record_logs', '201', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('254', '2', 'print_record_logs', '202', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('255', '2', 'print_record_logs', '203', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('256', '2', 'print_record_logs', '204', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('257', '2', 'print_record_logs', '205', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('258', '2', 'detail_records', '28', 'update', '2019-03-07 07:14:33');
INSERT INTO `project_activity_logs` VALUES ('259', '2', 'detail_records', '28', 'update', '2019-03-07 07:21:55');
INSERT INTO `project_activity_logs` VALUES ('260', '2', 'print_record_logs', '206', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('261', '2', 'detail_records', '29', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('262', '2', 'print_record_logs', '207', 'insert', '2019-03-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('263', '2', 'detail_records', '30', 'insert', '2019-03-08 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('264', '2', 'print_record_logs', '208', 'insert', '2019-03-08 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('265', '2', 'print_record_logs', '209', 'insert', '2019-03-08 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('266', '2', 'print_record_logs', '210', 'insert', '2019-03-08 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('267', '2', 'detail_records', '31', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('268', '2', 'print_record_logs', '211', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('269', '2', 'print_record_logs', '212', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('270', '2', 'detail_records', '32', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('271', '2', 'detail_records', '32', 'update', '2019-03-10 16:40:02');
INSERT INTO `project_activity_logs` VALUES ('272', '2', 'print_record_logs', '213', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('273', '2', 'print_record_logs', '214', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('274', '2', 'print_record_logs', '215', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('275', '2', 'print_record_logs', '216', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('276', '2', 'print_record_logs', '217', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('277', '2', 'print_record_logs', '218', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('278', '2', 'print_record_logs', '219', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('279', '2', 'print_record_logs', '220', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('280', '2', 'print_record_logs', '221', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('281', '2', 'print_record_logs', '222', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('282', '2', 'print_record_logs', '223', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('283', '2', 'print_record_logs', '224', 'insert', '2019-03-10 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('284', '2', 'print_record_logs', '225', 'insert', '2019-03-11 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('285', '2', 'print_record_logs', '226', 'insert', '2019-03-11 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('286', '1', 'print_record_logs', '227', 'insert', '2019-03-14 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('287', '2', 'detail_records', '33', 'insert', '2019-05-09 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('288', '2', 'detail_records', '33', 'update', '2019-05-09 12:29:57');
INSERT INTO `project_activity_logs` VALUES ('289', '2', 'print_record_logs', '228', 'insert', '2019-05-09 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('290', '2', 'print_record_logs', '229', 'insert', '2019-05-09 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('291', '2', 'print_record_logs', '230', 'insert', '2019-05-16 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('292', '2', 'print_record_logs', '231', 'insert', '2019-05-16 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('293', '2', 'print_record_logs', '232', 'insert', '2019-05-16 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('294', '2', 'print_record_logs', '233', 'insert', '2019-05-22 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('295', '2', 'print_record_logs', '234', 'insert', '2019-05-22 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('296', '2', 'detail_records', '34', 'insert', '2019-05-22 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('297', '2', 'detail_records', '34', 'update', '2019-05-22 11:34:40');
INSERT INTO `project_activity_logs` VALUES ('298', '2', 'print_record_logs', '235', 'insert', '2019-05-22 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('299', '2', 'print_record_logs', '236', 'insert', '2019-05-22 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('300', '2', 'print_record_logs', '237', 'insert', '2019-07-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('301', '2', 'print_record_logs', '238', 'insert', '2019-07-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('302', '1', 'detail_records', '35', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('303', '1', 'print_record_logs', '239', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('304', '1', 'detail_records', '35', 'update', '2019-07-27 07:35:09');
INSERT INTO `project_activity_logs` VALUES ('305', '1', 'print_record_logs', '240', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('306', '1', 'print_record_logs', '241', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('307', '1', 'print_record_logs', '242', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('308', '1', 'print_record_logs', '243', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('309', '1', 'print_record_logs', '244', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('310', '1', 'print_record_logs', '245', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('311', '1', 'detail_records', '35', 'update', '2019-07-27 08:12:30');
INSERT INTO `project_activity_logs` VALUES ('312', '1', 'print_record_logs', '246', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('313', '1', 'print_record_logs', '247', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('314', '1', 'print_record_logs', '248', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('315', '1', 'print_record_logs', '249', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('316', '1', 'print_record_logs', '250', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('317', '1', 'print_record_logs', '251', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('318', '1', 'print_record_logs', '252', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('319', '1', 'print_record_logs', '253', 'insert', '2019-07-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('320', '1', 'print_record_logs', '254', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('321', '1', 'print_record_logs', '255', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('322', '1', 'print_record_logs', '256', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('323', '1', 'print_record_logs', '257', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('324', '1', 'print_record_logs', '258', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('325', '1', 'print_record_logs', '259', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('326', '1', 'print_record_logs', '260', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('327', '1', 'print_record_logs', '261', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('328', '1', 'print_record_logs', '262', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('329', '1', 'print_record_logs', '263', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('330', '1', 'print_record_logs', '264', 'insert', '2019-08-21 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('331', '1', 'print_record_logs', '265', 'insert', '2019-10-23 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('332', '1', 'print_record_logs', '266', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('333', '1', 'print_record_logs', '267', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('334', '1', 'print_record_logs', '268', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('335', '1', 'print_record_logs', '269', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('336', '1', 'print_record_logs', '270', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('337', '1', 'print_record_logs', '271', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('338', '1', 'print_record_logs', '272', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('339', '1', 'print_record_logs', '273', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('340', '1', 'print_record_logs', '274', 'insert', '2019-10-24 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('341', '1', 'mst_cards', '16', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('342', '1', 'print_record_logs', '275', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('343', '1', 'print_record_logs', '276', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('344', '1', 'print_record_logs', '277', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('345', '1', 'print_record_logs', '278', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('346', '1', 'print_record_logs', '279', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('347', '1', 'print_record_logs', '280', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('348', '1', 'print_record_logs', '281', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('349', '1', 'print_record_logs', '282', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('350', '1', 'print_record_logs', '283', 'insert', '2019-11-19 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('351', '2', 'print_record_logs', '284', 'insert', '2019-11-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('352', '2', 'mst_cards', '17', 'insert', '2019-11-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('353', '2', 'mst_cards', '18', 'insert', '2019-11-27 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('354', '1', 'print_record_logs', '285', 'insert', '2020-12-11 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('355', '1', 'print_record_logs', '286', 'insert', '2020-12-11 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('356', '1', 'print_record_logs', '287', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('357', '1', 'print_record_logs', '288', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('358', '1', 'print_record_logs', '289', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('359', '1', 'print_record_logs', '290', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('360', '1', 'print_record_logs', '291', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('361', '1', 'print_record_logs', '292', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('362', '1', 'print_record_logs', '293', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('363', '1', 'print_record_logs', '294', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('364', '1', 'print_record_logs', '295', 'insert', '2021-04-07 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('365', '1', 'print_record_logs', '296', 'insert', '2021-06-30 00:00:00');
INSERT INTO `project_activity_logs` VALUES ('366', '1', 'print_record_logs', '297', 'insert', '2021-06-30 00:00:00');

-- ----------------------------
-- Table structure for project_audit_logs
-- ----------------------------
DROP TABLE IF EXISTS `project_audit_logs`;
CREATE TABLE `project_audit_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `table_pk` int(11) unsigned NOT NULL,
  `column_name` varchar(255) DEFAULT NULL,
  `old_value` varchar(1000) DEFAULT NULL,
  `new_value` varchar(1000) DEFAULT NULL,
  `action_dttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_audit_logs
-- ----------------------------
INSERT INTO `project_audit_logs` VALUES ('1', '2', 'detail_records', '3', 'image', 'card_20190219153108.JPG', 'card_20190219153230.JPG', '2019-02-19 15:32:44');
INSERT INTO `project_audit_logs` VALUES ('2', '2', 'detail_records', '3', 'blood_group', '', 'O+ve', '2019-02-19 15:32:44');
INSERT INTO `project_audit_logs` VALUES ('3', '1', 'detail_records', '3', 'date_of_birth', null, '1987-01-27', '2019-02-24 15:55:13');
INSERT INTO `project_audit_logs` VALUES ('4', '1', 'detail_records', '11', 'image', null, 'card_20190224163350.JPG', '2019-02-24 16:33:52');
INSERT INTO `project_audit_logs` VALUES ('5', '1', 'detail_records', '10', 'image', null, 'card_20190224163408.jpg', '2019-02-24 16:34:10');
INSERT INTO `project_audit_logs` VALUES ('6', '1', 'detail_records', '9', 'image', null, 'card_20190224163426.jpg', '2019-02-24 16:34:27');
INSERT INTO `project_audit_logs` VALUES ('7', '1', 'detail_records', '7', 'image', null, 'card_20190224163435.JPG', '2019-02-24 16:34:36');
INSERT INTO `project_audit_logs` VALUES ('8', '1', 'detail_records', '6', 'image', null, 'card_20190224163448.jpg', '2019-02-24 16:34:49');
INSERT INTO `project_audit_logs` VALUES ('9', '1', 'detail_records', '5', 'image', null, 'card_20190224163500.jpg', '2019-02-24 16:35:02');
INSERT INTO `project_audit_logs` VALUES ('10', '1', 'detail_records', '4', 'image', null, 'card_20190224163510.JPG', '2019-02-24 16:35:12');
INSERT INTO `project_audit_logs` VALUES ('11', '1', 'detail_records', '3', 'image', null, 'card_20190224163521.JPG', '2019-02-24 16:35:23');
INSERT INTO `project_audit_logs` VALUES ('12', '1', 'detail_records', '2', 'image', null, 'card_20190224163535.JPG', '2019-02-24 16:35:36');
INSERT INTO `project_audit_logs` VALUES ('13', '2', 'detail_records', '15', 'image', '', 'card_20190224183740.jpg', '2019-02-24 18:37:48');
INSERT INTO `project_audit_logs` VALUES ('14', '2', 'detail_records', '9', 'image', 'card_20190224163426.jpg', 'sfsf.png', '2019-02-27 09:04:04');
INSERT INTO `project_audit_logs` VALUES ('15', '2', 'detail_records', '21', 'name', '369', 'test', '2019-02-27 09:49:45');
INSERT INTO `project_audit_logs` VALUES ('16', '2', 'detail_records', '21', 'employee_number', 'test', '369', '2019-02-27 09:49:45');
INSERT INTO `project_audit_logs` VALUES ('17', '2', 'detail_records', '21', 'image', '', '369.png', '2019-02-27 09:53:33');
INSERT INTO `project_audit_logs` VALUES ('18', '2', 'detail_records', '22', 'name', 'PUJAN JUNG GURUNG', 'PUJAN J. GURUNG', '2019-02-27 10:19:05');
INSERT INTO `project_audit_logs` VALUES ('19', '2', 'detail_records', '22', 'image', '140598', '140598.png', '2019-02-27 10:19:05');
INSERT INTO `project_audit_logs` VALUES ('20', '2', 'detail_records', '24', 'image', '', '21167999.png', '2019-02-27 16:09:55');
INSERT INTO `project_audit_logs` VALUES ('21', '2', 'detail_records', '27', 'date_of_birth', '2019-03-03', '1964-12-06', '2019-03-03 16:06:55');
INSERT INTO `project_audit_logs` VALUES ('22', '2', 'detail_records', '27', 'department', '', 'G4s', '2019-03-03 16:06:55');
INSERT INTO `project_audit_logs` VALUES ('23', '2', 'detail_records', '28', 'image', '', '140598.png', '2019-03-07 07:14:33');
INSERT INTO `project_audit_logs` VALUES ('24', '2', 'detail_records', '28', 'id_no', '', '033', '2019-03-07 07:21:55');
INSERT INTO `project_audit_logs` VALUES ('25', '2', 'detail_records', '28', 'blood_group', '', '0 +ve', '2019-03-07 07:21:55');
INSERT INTO `project_audit_logs` VALUES ('26', '2', 'detail_records', '28', 'date_of_birth', '2019-03-07', '1982-11-08', '2019-03-07 07:21:55');
INSERT INTO `project_audit_logs` VALUES ('27', '2', 'detail_records', '32', 'name', 'Ravi Lohala', 'Rabi Lohala', '2019-03-10 16:40:02');
INSERT INTO `project_audit_logs` VALUES ('28', '2', 'detail_records', '32', 'image', null, '887755.png', '2019-03-10 16:40:02');
INSERT INTO `project_audit_logs` VALUES ('29', '2', 'detail_records', '33', 'employee_number', '', '123', '2019-05-09 12:29:57');
INSERT INTO `project_audit_logs` VALUES ('30', '2', 'detail_records', '34', 'name', 'nabika', 'winee', '2019-05-22 11:34:40');
INSERT INTO `project_audit_logs` VALUES ('31', '1', 'detail_records', '35', 'id_no', '', '228', '2019-07-27 07:35:09');
INSERT INTO `project_audit_logs` VALUES ('32', '1', 'detail_records', '35', 'department', '', 'Political', '2019-07-27 07:35:09');
INSERT INTO `project_audit_logs` VALUES ('33', '1', 'detail_records', '35', 'name', 'Sumit Sarma', 'Sumit Sharma Sameer', '2019-07-27 08:12:30');
INSERT INTO `project_audit_logs` VALUES ('34', '1', 'detail_records', '35', 'image', 'Sumit Sarma642339.png', 'Sumit Sharma Sameer642339.png', '2019-07-27 08:12:30');

-- ----------------------------
-- Table structure for project_migrations
-- ----------------------------
DROP TABLE IF EXISTS `project_migrations`;
CREATE TABLE `project_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_migrations
-- ----------------------------
INSERT INTO `project_migrations` VALUES ('14');

-- ----------------------------
-- Table structure for project_sessions
-- ----------------------------
DROP TABLE IF EXISTS `project_sessions`;
CREATE TABLE `project_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_sessions
-- ----------------------------
INSERT INTO `project_sessions` VALUES ('2fb4fabddb284408f1d7f6f4d55ed63864304d2d', '::1', '1635850504', '__ci_last_regenerate|i:1635850249;');
INSERT INTO `project_sessions` VALUES ('49fb12076a96a61a324811c79c3eb56535b99824', '::1', '1617787446', '__ci_last_regenerate|i:1617787177;');
INSERT INTO `project_sessions` VALUES ('6c724323db28412e440d2b76889fa712b3408df3', '::1', '1625033582', '__ci_last_regenerate|i:1625033529;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('897e2651990a41a2c46aa95af2513965916bd0a5', '::1', '1617787836', '__ci_last_regenerate|i:1617787568;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');
INSERT INTO `project_sessions` VALUES ('9c26723f824d25de4cec2de10c760b9a50992e45', '::1', '1617788152', '__ci_last_regenerate|i:1617788008;id|s:1:\"1\";username|s:10:\"superadmin\";email|s:20:\"superadmin@gmail.com\";loggedin|b:1;');

-- ----------------------------
-- View structure for view_group_permissions
-- ----------------------------
DROP VIEW IF EXISTS `view_group_permissions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_group_permissions` AS (select `gp`.`group_id` AS `group_id`,`gp`.`perm_id` AS `perm_id`,`perm`.`name` AS `permission`,`grp`.`name` AS `group_name` from ((`aauth_group_permissions` `gp` join `aauth_groups` `grp` on((`grp`.`id` = `gp`.`group_id`))) join `aauth_permissions` `perm` on((`perm`.`id` = `gp`.`perm_id`)))) ; ;

-- ----------------------------
-- View structure for view_users
-- ----------------------------
DROP VIEW IF EXISTS `view_users`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_users` AS (select `u`.`id` AS `id`,`u`.`email` AS `email`,`u`.`pass` AS `pass`,`u`.`username` AS `username`,`u`.`fullname` AS `fullname`,`u`.`banned` AS `banned`,`u`.`last_login` AS `last_login`,`u`.`last_activity` AS `last_activity`,`u`.`date_created` AS `date_created`,`u`.`forgot_exp` AS `forgot_exp`,`u`.`remember_time` AS `remember_time`,`u`.`remember_exp` AS `remember_exp`,`u`.`verification_code` AS `verification_code`,`u`.`totp_secret` AS `totp_secret`,`u`.`ip_address` AS `ip_address` from `aauth_users` `u`) ; ;

-- ----------------------------
-- View structure for view_user_groups
-- ----------------------------
DROP VIEW IF EXISTS `view_user_groups`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_user_groups` AS (select `ug`.`user_id` AS `user_id`,`ug`.`group_id` AS `group_id`,`g`.`name` AS `group_name`,`u`.`username` AS `username`,`u`.`email` AS `email`,`u`.`fullname` AS `fullname` from ((`aauth_user_groups` `ug` join `aauth_users` `u` on((`u`.`id` = `ug`.`user_id`))) join `aauth_groups` `g` on((`g`.`id` = `ug`.`group_id`)))) ; ;

-- ----------------------------
-- View structure for view_user_permissions
-- ----------------------------
DROP VIEW IF EXISTS `view_user_permissions`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `view_user_permissions` AS (select `up`.`user_id` AS `user_id`,`up`.`perm_id` AS `perm_id`,`perm`.`name` AS `permission`,`u`.`username` AS `username`,`u`.`email` AS `email`,`u`.`fullname` AS `fullname` from ((`aauth_user_permissions` `up` join `aauth_users` `u` on((`u`.`id` = `up`.`user_id`))) join `aauth_permissions` `perm` on((`perm`.`id` = `up`.`perm_id`)))) ; ;
