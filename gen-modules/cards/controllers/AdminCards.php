<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Cards
 *
 * Extends the Project_Controller class
 * 
 */

class AdminCards extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Cards');

        $this->load->model('cards/card_model');
        $this->lang->load('cards/card');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('cards');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'cards';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->card_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->card_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->card_model->insert($data);
        }
        else
        {
            $success=$this->card_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_at'] = $this->input->post('created_at');
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['card_type'] = $this->input->post('card_type');
		$data['image'] = $this->input->post('image');

        return $data;
   }
}