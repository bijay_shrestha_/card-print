<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('cards'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('cards'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridCardToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridCardInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridCardFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridCard"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowCard">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-cards', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "cards_id"/>
            <table class="form-table">
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><div id='created_at' class='date_box' name='created_at'></div></td>
				</tr>
				<tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><div id='updated_at' class='date_box' name='updated_at'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><div id='deleted_at' class='number_general' name='deleted_at'></div></td>
				</tr>
				<tr>
					<td><label for='card_type'><?php echo lang('card_type')?></label></td>
					<td><input id='card_type' class='text_input' name='card_type'></td>
				</tr>
				<tr>
					<td><label for='image'><?php echo lang('image')?></label></td>
					<td><input id='image' class='text_input' name='image'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxCardSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxCardCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var cardsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_at', type: 'number' },
			{ name: 'card_type', type: 'string' },
			{ name: 'image', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/cards/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	cardsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridCard").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridCard").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridCard").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: cardsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridCardToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editCardRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("card_type"); ?>',datafield: 'card_type',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("image"); ?>',datafield: 'image',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridCard").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridCardFilterClear', function () { 
		$('#jqxGridCard').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridCardInsert', function () { 
		openPopupWindow('jqxPopupWindowCard', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowCard").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowCard").on('close', function () {
        reset_form_cards();
    });

    $("#jqxCardCancelButton").on('click', function () {
        reset_form_cards();
        $('#jqxPopupWindowCard').jqxWindow('close');
    });

    /*$('#form-cards').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_at', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_at').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#card_type', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#card_type').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#image', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#image').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxCardSubmitButton").on('click', function () {
        saveCardRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveCardRecord();
                }
            };
        $('#form-cards').jqxValidator('validate', validationResult);
        */
    });
});

function editCardRecord(index){
    var row =  $("#jqxGridCard").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#cards_id').val(row.id);
        $('#created_at').jqxDateTimeInput('setDate', row.created_at);
		$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_at').jqxDateTimeInput('setDate', row.updated_at);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_at').jqxNumberInput('val', row.deleted_at);
		$('#card_type').val(row.card_type);
		$('#image').val(row.image);
		
        openPopupWindow('jqxPopupWindowCard', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveCardRecord(){
    var data = $("#form-cards").serialize();
	
	$('#jqxPopupWindowCard').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/cards/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_cards();
                $('#jqxGridCard').jqxGrid('updatebounddata');
                $('#jqxPopupWindowCard').jqxWindow('close');
            }
            $('#jqxPopupWindowCard').unblock();
        }
    });
}

function reset_form_cards(){
	$('#cards_id').val('');
    $('#form-cards')[0].reset();
}
</script>