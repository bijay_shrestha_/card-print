<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Records
 *
 * Extends the Project_Controller class
 * 
 */

class AdminRecords extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Records');

        $this->load->model('records/record_model');
        $this->lang->load('records/record');
    }

	public function index()
	{
		// Display Page
		$data['header'] = lang('records');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'records';
		$this->load->view($this->_container,$data);
	}

	public function json()
	{
		search_params();
		
		$total=$this->record_model->find_count();
		
		paging('id');
		
		search_params();
		
		$rows=$this->record_model->findAll();
		
		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->record_model->insert($data);
        }
        else
        {
            $success=$this->record_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		$data['created_at'] = $this->input->post('created_at');
		$data['created_by'] = $this->input->post('created_by');
		$data['updated_at'] = $this->input->post('updated_at');
		$data['updated_by'] = $this->input->post('updated_by');
		$data['deleted_at'] = $this->input->post('deleted_at');
		$data['deleted_by'] = $this->input->post('deleted_by');
		$data['name'] = $this->input->post('name');
		$data['id_no'] = $this->input->post('id_no');
		$data['employee_number'] = $this->input->post('employee_number');
		$data['image'] = $this->input->post('image');
		$data['blood_group'] = $this->input->post('blood_group');
		$data['date_of_birth'] = $this->input->post('date_of_birth');
		$data['department'] = $this->input->post('department');

        return $data;
   }
}