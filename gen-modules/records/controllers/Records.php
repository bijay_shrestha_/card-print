<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Records
 *
 * Extends the Public_Controller class
 * 
 */

class Records extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Records');

        $this->load->model('records/record_model');
        $this->lang->load('records/record');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('records');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'records';
		$this->load->view($this->_container,$data);
	}
}