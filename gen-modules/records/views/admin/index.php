<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('records'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('records'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridRecordToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridRecordInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridRecordFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridRecord"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowRecord">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-records', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "records_id"/>
            <table class="form-table">
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><div id='created_at' class='date_box' name='created_at'></div></td>
				</tr>
				<tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><div id='updated_at' class='date_box' name='updated_at'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><div id='deleted_at' class='date_box' name='deleted_at'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='records_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></td>
					<td><input id='records_name' class='text_input' name='name'></td>
				</tr>
				<tr>
					<td><label for='id_no'><?php echo lang('id_no')?></label></td>
					<td><input id='id_no' class='text_input' name='id_no'></td>
				</tr>
				<tr>
					<td><label for='employee_number'><?php echo lang('employee_number')?></label></td>
					<td><input id='employee_number' class='text_input' name='employee_number'></td>
				</tr>
				<tr>
					<td><label for='image'><?php echo lang('image')?></label></td>
					<td><input id='image' class='text_input' name='image'></td>
				</tr>
				<tr>
					<td><label for='blood_group'><?php echo lang('blood_group')?></label></td>
					<td><input id='blood_group' class='text_input' name='blood_group'></td>
				</tr>
				<tr>
					<td><label for='date_of_birth'><?php echo lang('date_of_birth')?></label></td>
					<td><div id='date_of_birth' class='date_box' name='date_of_birth'></div></td>
				</tr>
				<tr>
					<td><label for='department'><?php echo lang('department')?></label></td>
					<td><input id='department' class='text_input' name='department'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxRecordSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxRecordCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var recordsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'name', type: 'string' },
			{ name: 'id_no', type: 'string' },
			{ name: 'employee_number', type: 'string' },
			{ name: 'image', type: 'string' },
			{ name: 'blood_group', type: 'string' },
			{ name: 'date_of_birth', type: 'date' },
			{ name: 'department', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/records/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	recordsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridRecord").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridRecord").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridRecord").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: recordsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridRecordToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editRecordRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("id_no"); ?>',datafield: 'id_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("employee_number"); ?>',datafield: 'employee_number',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("image"); ?>',datafield: 'image',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("blood_group"); ?>',datafield: 'blood_group',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("date_of_birth"); ?>',datafield: 'date_of_birth',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("department"); ?>',datafield: 'department',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridRecord").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridRecordFilterClear', function () { 
		$('#jqxGridRecord').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridRecordInsert', function () { 
		openPopupWindow('jqxPopupWindowRecord', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowRecord").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowRecord").on('close', function () {
        reset_form_records();
    });

    $("#jqxRecordCancelButton").on('click', function () {
        reset_form_records();
        $('#jqxPopupWindowRecord').jqxWindow('close');
    });

    /*$('#form-records').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#records_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#records_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#id_no', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#id_no').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#employee_number', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#employee_number').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#image', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#image').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#blood_group', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#blood_group').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#department', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#department').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxRecordSubmitButton").on('click', function () {
        saveRecordRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveRecordRecord();
                }
            };
        $('#form-records').jqxValidator('validate', validationResult);
        */
    });
});

function editRecordRecord(index){
    var row =  $("#jqxGridRecord").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#records_id').val(row.id);
        $('#created_at').jqxDateTimeInput('setDate', row.created_at);
		$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_at').jqxDateTimeInput('setDate', row.updated_at);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_at').jqxDateTimeInput('setDate', row.deleted_at);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#records_name').val(row.name);
		$('#id_no').val(row.id_no);
		$('#employee_number').val(row.employee_number);
		$('#image').val(row.image);
		$('#blood_group').val(row.blood_group);
		$('#date_of_birth').jqxDateTimeInput('setDate', row.date_of_birth);
		$('#department').val(row.department);
		
        openPopupWindow('jqxPopupWindowRecord', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function saveRecordRecord(){
    var data = $("#form-records").serialize();
	
	$('#jqxPopupWindowRecord').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/records/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_records();
                $('#jqxGridRecord').jqxGrid('updatebounddata');
                $('#jqxPopupWindowRecord').jqxWindow('close');
            }
            $('#jqxPopupWindowRecord').unblock();
        }
    });
}

function reset_form_records(){
	$('#records_id').val('');
    $('#form-records')[0].reset();
}
</script>