<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('print_record_logs'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('print_record_logs'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridPrint_record_logToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridPrint_record_logInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridPrint_record_logFilterClear"><?php echo lang('general_clear'); ?></button>
				</div>
				<div id="jqxGridPrint_record_log"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowPrint_record_log">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-print_record_logs', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "print_record_logs_id"/>
            <table class="form-table">
				<tr>
					<td><label for='created_at'><?php echo lang('created_at')?></label></td>
					<td><div id='created_at' class='date_box' name='created_at'></div></td>
				</tr>
				<tr>
					<td><label for='created_by'><?php echo lang('created_by')?></label></td>
					<td><div id='created_by' class='number_general' name='created_by'></div></td>
				</tr>
				<tr>
					<td><label for='updated_at'><?php echo lang('updated_at')?></label></td>
					<td><div id='updated_at' class='date_box' name='updated_at'></div></td>
				</tr>
				<tr>
					<td><label for='updated_by'><?php echo lang('updated_by')?></label></td>
					<td><div id='updated_by' class='number_general' name='updated_by'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_at'><?php echo lang('deleted_at')?></label></td>
					<td><div id='deleted_at' class='date_box' name='deleted_at'></div></td>
				</tr>
				<tr>
					<td><label for='deleted_by'><?php echo lang('deleted_by')?></label></td>
					<td><div id='deleted_by' class='number_general' name='deleted_by'></div></td>
				</tr>
				<tr>
					<td><label for='id_no'><?php echo lang('id_no')?></label></td>
					<td><input id='id_no' class='text_input' name='id_no'></td>
				</tr>
				<tr>
					<td><label for='card_id'><?php echo lang('card_id')?></label></td>
					<td><input id='card_id' class='text_input' name='card_id'></td>
				</tr>
                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxPrint_record_logSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxPrint_record_logCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){

	var print_record_logsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'id_no', type: 'string' },
			{ name: 'card_id', type: 'string' },
			
        ],
		url: '<?php echo site_url("admin/print_record_logs/json"); ?>',
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	print_record_logsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridPrint_record_log").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridPrint_record_log").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridPrint_record_log").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: print_record_logsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridPrint_record_logToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var e = '<a href="javascript:void(0)" onclick="editPrint_record_logRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
				}
			},
			{ text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			{ text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("id_no"); ?>',datafield: 'id_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("card_id"); ?>',datafield: 'card_id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridPrint_record_log").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridPrint_record_logFilterClear', function () { 
		$('#jqxGridPrint_record_log').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridPrint_record_logInsert', function () { 
		openPopupWindow('jqxPopupWindowPrint_record_log', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowPrint_record_log").jqxWindow({ 
        theme: theme,
        width: '75%',
        maxWidth: '75%',
        height: '75%',  
        maxHeight: '75%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowPrint_record_log").on('close', function () {
        reset_form_print_record_logs();
    });

    $("#jqxPrint_record_logCancelButton").on('click', function () {
        reset_form_print_record_logs();
        $('#jqxPopupWindowPrint_record_log').jqxWindow('close');
    });

    /*$('#form-print_record_logs').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#id_no', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#id_no').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#card_id', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#card_id').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxPrint_record_logSubmitButton").on('click', function () {
        savePrint_record_logRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   savePrint_record_logRecord();
                }
            };
        $('#form-print_record_logs').jqxValidator('validate', validationResult);
        */
    });
});

function editPrint_record_logRecord(index){
    var row =  $("#jqxGridPrint_record_log").jqxGrid('getrowdata', index);
  	if (row) {
  		$('#print_record_logs_id').val(row.id);
        $('#created_at').jqxDateTimeInput('setDate', row.created_at);
		$('#created_by').jqxNumberInput('val', row.created_by);
		$('#updated_at').jqxDateTimeInput('setDate', row.updated_at);
		$('#updated_by').jqxNumberInput('val', row.updated_by);
		$('#deleted_at').jqxDateTimeInput('setDate', row.deleted_at);
		$('#deleted_by').jqxNumberInput('val', row.deleted_by);
		$('#id_no').val(row.id_no);
		$('#card_id').val(row.card_id);
		
        openPopupWindow('jqxPopupWindowPrint_record_log', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function savePrint_record_logRecord(){
    var data = $("#form-print_record_logs").serialize();
	
	$('#jqxPopupWindowPrint_record_log').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/print_record_logs/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_print_record_logs();
                $('#jqxGridPrint_record_log').jqxGrid('updatebounddata');
                $('#jqxPopupWindowPrint_record_log').jqxWindow('close');
            }
            $('#jqxPopupWindowPrint_record_log').unblock();
        }
    });
}

function reset_form_print_record_logs(){
	$('#print_record_logs_id').val('');
    $('#form-print_record_logs')[0].reset();
}
</script>