<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


class Print_record_log_model extends MY_Model
{

    protected $_table = 'print_record_logs';

    protected $blamable = TRUE;

}