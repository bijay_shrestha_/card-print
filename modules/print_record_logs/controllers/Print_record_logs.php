<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Print_record_logs
 *
 * Extends the Public_Controller class
 * 
 */

class Print_record_logs extends Public_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Print Record Logs');

        $this->load->model('print_record_logs/print_record_log_model');
        $this->lang->load('print_record_logs/print_record_log');
    }

    public function index()
	{
		// Display Page
		$data['header'] = lang('print_record_logs');
		$data['page'] = $this->config->item('template_public') . "index";
		$data['module'] = 'print_record_logs';
		$this->load->view($this->_container,$data);
	}
}