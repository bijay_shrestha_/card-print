<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('records'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('records'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-body">
				<!-- row -->
				<form action="<?php echo site_url('admin/records/excel_upload')?>" method="post" enctype="multipart/form-data" >
					<div class="row">
						<div class="col-md-12 form-group">
								<input type="file" name="userfile">
						</div>
						<div class="col-md-12 form-group">
							<button class="btn btn-primary">Submit</button>
						</div>
					</div>
				</form>
			</div>

		</div>
	</section>
</div>