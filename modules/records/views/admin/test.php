<link href="<?php echo site_url()?>assets/css/croppic.css" rel="stylesheet">
<style type="text/css">
	#cropContainerMinimal {
			width: 200px;
			height: 150px;
			border: 1px;
			border-color: #0000;
			position:relative; /* or fixed or absolute */
		}
</style>


<div class="content-wrapper">
	<h4 class="centered"> Minimal Controls </h4>
	<p class="centered">( define the controls available )</p>
	<div id="cropContainerMinimal"></div>
</div>

<script src="<?php echo site_url()?>assets/js/image_crop/jquery.mousewheel.min.js"></script>
<script src="<?php echo site_url()?>assets/js/image_crop/croppic.min.js"></script>
<!-- <script src="<?php echo site_url()?>assets/js/image_crop/crop-main.js"></script> -->

<script type="text/javascript">
	// $(function(){
		var croppicContaineroutputMinimal = {
			uploadUrl:'<?php echo site_url()?>admin/records/testimg_save_to_file',
			cropUrl:'img_crop_to_file.php', 
			modal:false,
			doubleZoomControls:false,
		    rotateControls: false,
			loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
			onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
			onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
			onImgDrag: function(){ console.log('onImgDrag') },
			onImgZoom: function(){ console.log('onImgZoom') },
			onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
			onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
			onReset:function(){ console.log('onReset') },
			onError:function(errormessage){ console.log('onError:'+errormessage) }
		}
		var cropContaineroutput = new Croppic('cropContainerMinimal', croppicContaineroutputMinimal);
	// });
</script>