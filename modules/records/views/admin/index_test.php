
<link href="https://fengyuanchen.github.io/cropperjs/css/cropper.css" rel="stylesheet"/>
<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100px;
}
.page {
	margin: 1em auto;
	max-width: 768px;
	display: flex;
	align-items: flex-start;
	flex-wrap: wrap;
	height: 100%;
}

.box {
	padding: 0.5em;
	width: 100%;
	margin:0.5em;
}

.box-2 {
	padding: 0.5em;
	width: calc(100%/2 - 1em);
}

.options label,
.options input{
	width:4em;
	padding:0.5em 1em;
}


.hide {
	display: none;
}

img {
	max-width: 100%;
}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('records'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('records'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- row -->
		<div class="row">
			<div class="col-xs-12 connectedSortable">
				<?php echo displayStatus(); ?>
				<div id='jqxGridRecordToolbar' class='grid-toolbar'>
					<button type="button" class="btn btn-primary btn-flat btn-xs" id="jqxGridRecordInsert"><?php echo lang('general_create'); ?></button>
					<button type="button" class="btn btn-danger btn-flat btn-xs" id="jqxGridRecordFilterClear"><?php echo lang('general_clear'); ?></button> | 
					<a href="<?php echo site_url('admin/records/index/BE Staff')?>"><button class="btn btn-primary btn-flat btn-xs" >BE Staff</button></a>
					<a href="<?php echo site_url('admin/records/index/Family')?>"><button class="btn btn-primary btn-flat btn-xs" >Family</button></a>
					<a href="<?php echo site_url('admin/records/index/House')?>"><button class="btn btn-primary btn-flat btn-xs" >House</button></a>
					<a href="<?php echo site_url('admin/records/index/Temporary')?>"><button class="btn btn-primary btn-flat btn-xs" >Temporary</button></a>
					<a href="<?php echo site_url('admin/records/index/Contractor')?>"><button class="btn btn-primary btn-flat btn-xs" >Contractor</button></a>
					<a href="<?php echo site_url('admin/records/index/FCO Visitor')?>"><button class="btn btn-primary btn-flat btn-xs" >FCO Visitor</button></a>
				</div>
				<div id="jqxGridRecord"></div>
			</div><!-- /.col -->
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="jqxPopupWindowRecord">
   <div class='jqxExpander-custom-div'>
        <span class='popup_title' id="window_poptup_title"></span>
    </div>
    <div class="form_fields_area">
        <?php echo form_open('', array('id' =>'form-records', 'onsubmit' => 'return false')); ?>
        	<input type = "hidden" name = "id" id = "records_id"/>
            <table class="form-table">
				<tr>
					<td><label for='id_no'><?php echo lang('id_no')?></label></td>
					<td><input id='id_no' class='text_input' name='id_no'></td>
				</tr>
				<tr>
					<td><label for='records_name'><?php echo lang('name')?><span class='mandatory'>*</span></label></td>
					<td><input id='records_name' class='text_input' name='name'></td>
				</tr>
				<tr>
					<td><label for='employee_number'><?php echo lang('employee_number')?></label></td>
					<td><input id='employee_number' class='text_input' name='employee_number'></td>
				</tr>
				<tr>
					<td><label for='image'><?php echo lang('image')?></label></td>
					<td>
						<!-- <input id='image' class='text_input' name='image'> -->
						<!-- <input type="file" id="image_input">
						<input type="hidden" id="image" name="image"> -->

						<div class="box">
							<input type="file" id="file-input">
						</div>
						<!-- leftbox -->
						<div class="box-2">
							<div class="result"></div>
						</div>
						<!--rightbox-->
						<div class="box-2 img-result hide">
							<!-- result of crop -->
							<img class="cropped" src="" alt="">
						</div>
						<!-- input file -->
						<div class="box">
							<div class="options hide">
								<label> Width</label>
								<input type="number" class="img-w" value="300" min="100" max="1200" />
							</div>
							<!-- save btn -->
							<button class="btn save hide">Save</button>
							<!-- download btn -->
							<a href="" class="btn download hide">Download</a>
						</div>
					</td>
					<td>
						<!-- <img id='img-upload'/> -->
						<div id="img-upload"></div>
					</td>
				</tr>
				<tr>
					<td><label for='blood_group'><?php echo lang('blood_group')?></label></td>
					<td><input id='blood_group' class='text_input' name='blood_group'></td>
				</tr>
				<tr>
					<td><label for='date_of_birth'><?php echo lang('date_of_birth')?></label></td>
					<td><div id='date_of_birth' class='date_box' name='date_of_birth'></div></td>
				</tr>
				<tr>
					<td><label for='department'><?php echo lang('department')?></label></td>
					<td><input id='department' class='text_input' name='department'></td>
				</tr>
				<tr>
					<td><label for='record_type'><?php echo lang('record_type')?></label></td>
					<td><div id='record_type' name='record_type'></div></td>
				</tr>

                <tr>
                    <th colspan="2">
                        <button type="button" class="btn btn-success btn-xs btn-flat" id="jqxRecordSubmitButton"><?php echo lang('general_save'); ?></button>
                        <button type="button" class="btn btn-default btn-xs btn-flat" id="jqxRecordCancelButton"><?php echo lang('general_cancel'); ?></button>
                    </th>
                </tr>
               
          </table>
        <?php echo form_close(); ?>
    </div>
</div>

<script language="javascript" type="text/javascript">

$(function(){
	// upload();

	var record_typeSource = [
      "BE Staff",
      "Family",
      "House",
      "Temporary",
      "Contractor",
      "FCO Visitor",
      ];
      $("#record_type").jqxComboBox({ 
        source: record_typeSource, 
        width: '200px', 
        height: '25px',
        placeHolder : 'Select a value'
    });

	$("#date_of_birth").jqxDateTimeInput({
     	allowNullDate: true
 	});

 	var url = '<?php echo site_url("admin/records/json/$record_type"); ?>';

	var recordsDataSource =
	{
		datatype: "json",
		datafields: [
			{ name: 'id', type: 'number' },
			{ name: 'created_at', type: 'date' },
			{ name: 'created_by', type: 'number' },
			{ name: 'updated_at', type: 'date' },
			{ name: 'updated_by', type: 'number' },
			{ name: 'deleted_at', type: 'date' },
			{ name: 'deleted_by', type: 'number' },
			{ name: 'name', type: 'string' },
			{ name: 'id_no', type: 'string' },
			{ name: 'employee_number', type: 'string' },
			{ name: 'image', type: 'string' },
			{ name: 'blood_group', type: 'string' },
			{ name: 'date_of_birth', type: 'date' },
			{ name: 'department', type: 'string' },
			{ name: 'record_type', type: 'string' },
			
        ],
		url: url,
		pagesize: defaultPageSize,
		root: 'rows',
		id : 'id',
		cache: true,
		pager: function (pagenum, pagesize, oldpagenum) {
        	//callback called when a page or page size is changed.
        },
        beforeprocessing: function (data) {
        	recordsDataSource.totalrecords = data.total;
        },
	    // update the grid and send a request to the server.
	    filter: function () {
	    	$("#jqxGridRecord").jqxGrid('updatebounddata', 'filter');
	    },
	    // update the grid and send a request to the server.
	    sort: function () {
	    	$("#jqxGridRecord").jqxGrid('updatebounddata', 'sort');
	    },
	    processdata: function(data) {
	    }
	};
	
	$("#jqxGridRecord").jqxGrid({
		theme: theme,
		width: '100%',
		height: gridHeight,
		source: recordsDataSource,
		altrows: true,
		pageable: true,
		sortable: true,
		rowsheight: 30,
		columnsheight:30,
		showfilterrow: true,
		filterable: true,
		columnsresize: true,
		autoshowfiltericon: true,
		columnsreorder: true,
		selectionmode: 'none',
		virtualmode: true,
		enableanimations: false,
		pagesizeoptions: pagesizeoptions,
		showtoolbar: true,
		rendertoolbar: function (toolbar) {
			var container = $("<div style='margin: 5px; height:50px'></div>");
			container.append($('#jqxGridRecordToolbar').html());
			toolbar.append(container);
		},
		columns: [
			{ text: 'SN', width: 50, pinned: true, exportable: false,  columntype: 'number', cellclassname: 'jqx-widget-header', renderer: gridColumnsRenderer, cellsrenderer: rownumberRenderer , filterable: false},
			{
				text: 'Action', datafield: 'action', width:75, sortable:false,filterable:false, pinned:true, align: 'center' , cellsalign: 'center', cellclassname: 'grid-column-center', 
				cellsrenderer: function (index) {
					var row =  $("#jqxGridRecord").jqxGrid('getrowdata', index);
					console.log(row);
					var e = '<a href="javascript:void(0)" onclick="editRecordRecord(' + index + '); return false;" title="Edit"><i class="fa fa-edit"></i></a>';
					var p = '<a href="<?php echo site_url('admin/records/select_card')?>/'+row.id_no+'/'+row.employee_number+'" title="Print"><i class="fa fa-print"></i></a>';
					var d = '<a href="javascript:void(0)" onclick="deleteRecordRecord(' + index + '); return false;" title="Delete"><i class="fa fa-trash"></i></a>';
					// return '<div style="text-align: center; margin-top: 8px;">' + e + '</div>';
					return '<div style="text-align: center; margin-top: 8px;">' + e + ' | ' + p + ' | ' + d + '</div>';
				}
			},
			// { text: '<?php echo lang("id"); ?>',datafield: 'id',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("created_at"); ?>',datafield: 'created_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			// { text: '<?php echo lang("created_by"); ?>',datafield: 'created_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("updated_at"); ?>',datafield: 'updated_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			// { text: '<?php echo lang("updated_by"); ?>',datafield: 'updated_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("deleted_at"); ?>',datafield: 'deleted_at',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			// { text: '<?php echo lang("deleted_by"); ?>',datafield: 'deleted_by',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("id_no"); ?>',datafield: 'id_no',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("name"); ?>',datafield: 'name',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("employee_number"); ?>',datafield: 'employee_number',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("record_type"); ?>',datafield: 'record_type',width: 150,filterable: true,renderer: gridColumnsRenderer },
			// { text: '<?php echo lang("image"); ?>',datafield: 'image',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("department"); ?>',datafield: 'department',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("blood_group"); ?>',datafield: 'blood_group',width: 150,filterable: true,renderer: gridColumnsRenderer },
			{ text: '<?php echo lang("date_of_birth"); ?>',datafield: 'date_of_birth',width: 150,filterable: true,renderer: gridColumnsRenderer, columntype: 'date', filtertype: 'range', cellsformat:  formatString_yyyy_MM_dd},
			
		],
		rendergridrows: function (result) {
			return result.data;
		}
	});

	$("[data-toggle='offcanvas']").click(function(e) {
	    e.preventDefault();
	    setTimeout(function() {$("#jqxGridRecord").jqxGrid('refresh');}, 500);
	});

	$(document).on('click','#jqxGridRecordFilterClear', function () { 
		$('#jqxGridRecord').jqxGrid('clearfilters');
	});

	$(document).on('click','#jqxGridRecordInsert', function () { 
 		$("#jqxDateTimeInput").val(null);
	    $('#img-upload').html('');
		openPopupWindow('jqxPopupWindowRecord', '<?php echo lang("general_add")  . "&nbsp;" .  $header; ?>');
    });

	// initialize the popup window
    $("#jqxPopupWindowRecord").jqxWindow({ 
        theme: theme,
        width: '90%',
        maxWidth: '90%',
        height: '90%',  
        maxHeight: '90%',  
        isModal: true, 
        autoOpen: false,
        modalOpacity: 0.7,
        showCollapseButton: false 
    });

    $("#jqxPopupWindowRecord").on('close', function () {
        reset_form_records();
 		$("#jqxDateTimeInput").val(null);
    });

    $("#jqxRecordCancelButton").on('click', function () {
        reset_form_records();
        $('#jqxPopupWindowRecord').jqxWindow('close');
    });

    /*$('#form-records').jqxValidator({
        hintType: 'label',
        animationDuration: 500,
        rules: [
			{ input: '#created_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#created_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#updated_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#updated_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#deleted_by', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#deleted_by').jqxNumberInput('val');
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#records_name', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#records_name').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#id_no', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#id_no').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#employee_number', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#employee_number').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#image', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#image').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#blood_group', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#blood_group').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

			{ input: '#department', message: 'Required', action: 'blur', 
				rule: function(input) {
					val = $('#department').val();
					return (val == '' || val == null || val == 0) ? false: true;
				}
			},

        ]
    });*/

    $("#jqxRecordSubmitButton").on('click', function () {
        saveRecordRecord();
        /*
        var validationResult = function (isValid) {
                if (isValid) {
                   saveRecordRecord();
                }
            };
        $('#form-records').jqxValidator('validate', validationResult);
        */
    });
});

function editRecordRecord(index){
    var row =  $("#jqxGridRecord").jqxGrid('getrowdata', index);
    $('#img-upload').html('');
 	$("#jqxDateTimeInput").val(null);
  	if (row) {
  		$('#records_id').val(row.id);
		$('#records_name').val(row.name);
		$('#id_no').val(row.id_no);
		$('#employee_number').val(row.employee_number);
		$('#image').val(row.image);
		$('#blood_group').val(row.blood_group);
		$('#date_of_birth').jqxDateTimeInput('setDate', row.date_of_birth);
		$('#department').val(row.department);
		$('#record_type').val(row.record_type);
		$('#img-upload').html("<img src='<?php echo base_url()?>/uploads/detail/" + row.image +"' style='height:100px;'>");
		
        openPopupWindow('jqxPopupWindowRecord', '<?php echo lang("general_edit")  . "&nbsp;" .  $header; ?>');
    }
}

function deleteRecordRecord(index){
    var row =  $("#jqxGridRecord").jqxGrid('getrowdata', index);
    if(confirm('Do you want to delete this data.')){
	    $.post('<?php echo site_url("admin/records/delete")?>'+ '/' + row.id, function(result){
	    	if(result.success){
	    		$('#jqxGridRecord').jqxGrid('updatebounddata');
	    	}else{
	    		alert('Error Occured')
	    	}
	    },'json');
    }
}

function saveRecordRecord(){
    var data = $("#form-records").serialize();
	
	$('#jqxPopupWindowRecord').block({ 
        message: '<span>Processing your request. Please be patient.</span>',
        css: { 
            width                   : '75%',
            border                  : 'none', 
            padding                 : '50px', 
            backgroundColor         : '#000', 
            '-webkit-border-radius' : '10px', 
            '-moz-border-radius'    : '10px', 
            opacity                 : .7, 
            color                   : '#fff',
            cursor                  : 'wait' 
        }, 
    });

    $.ajax({
        type: "POST",
        url: '<?php echo site_url("admin/records/save"); ?>',
        data: data,
        success: function (result) {
            var result = eval('('+result+')');
            if (result.success) {
                reset_form_records();
                $('#jqxGridRecord').jqxGrid('updatebounddata');
                $('#jqxPopupWindowRecord').jqxWindow('close');
            }
            $('#jqxPopupWindowRecord').unblock();
        }
    });
}

function reset_form_records(){
	$('#records_id').val('');
    $('#form-records')[0].reset();
}
</script>

<script type="text/javascript">
	// function upload()
	// {
	//     uploader=$('#image_input');
	//     new AjaxUpload(uploader, {
	//       action: '<?php echo site_url('admin/records/do_upload')?>',
	//       name: 'userfile',
	//       responseType: "json",
	//       onSubmit: function(file, ext){
	//          if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
	//                     // extension is not allowed 
	//           alert('Only JPG, PNG or GIF files are allowed');
	//           return false;
	//         }
	//         //status.text('Uploading...');
	//         $('#signiature-status').html('Image is uploading....');
	//       },
	//       onComplete: function(file, response){
	//         if(response.success){
	//           var filename = response.file_name;
	//           $('#img-upload').html("<img src='<?php echo base_url()?>/uploads/detail/" + response.file_name +"' style='height:100px;'>");
	//           $('#image').val(filename);
	//         }
	//       }   
	//     });   
 //  	}
</script>

<script language="javascript" type="text/javascript">

// vars
let result = document.querySelector('.result'),
img_result = document.querySelector('.img-result'),
img_w = document.querySelector('.img-w'),
img_h = document.querySelector('.img-h'),
options = document.querySelector('.options'),
save = document.querySelector('.save'),
cropped = document.querySelector('.cropped'),
dwn = document.querySelector('.download'),
upload = document.querySelector('#file-input'),
cropper = '';

	// on change show image with crop options
	upload.addEventListener('change', (e) => {
		if (e.target.files.length) {
			// start file reader
			const reader = new FileReader();
			reader.onload = (e)=> {
				if(e.target.result){
					// create new image
					let img = document.createElement('img');
					img.id = 'image';
					img.src = e.target.result
					// clean result before
					result.innerHTML = '';
					// append new image
					result.appendChild(img);
					// show save btn and options
					save.classList.remove('hide');
					options.classList.remove('hide');
					// init cropper
					cropper = new Cropper(img);
				}
			};
			reader.readAsDataURL(e.target.files[0]);
		}
	});

	// save on click
	save.addEventListener('click',(e)=>{
		e.preventDefault();
	  	// get result to data uri
	  	let imgSrc = cropper.getCroppedCanvas({
			width: img_w.value // input value
		}).toDataURL();
	  	// remove hide class of img
	  	cropped.classList.remove('hide');
	  	img_result.classList.remove('hide');
		// show image cropped
		cropped.src = imgSrc;
	  	// console.log(imgSrc);
	  	var record_name='sfsf';
	  	$.ajax({
		  	type: "POST",
		  	url: "<?php echo site_url('admin/records/do_upload1')?>",
		  	data: { 
		  		imgBase64: imgSrc,
		  		record_name: record_name,

		  	}
	  	}).done(function(o) {
	  		console.log(o);
	  	});
	  	dwn.classList.remove('hide');
	  	dwn.download = 'imagename.png';
	  	dwn.setAttribute('href',imgSrc);
	});


</script>