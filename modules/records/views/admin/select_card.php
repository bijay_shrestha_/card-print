<style type="text/css">
	/*.preview {
		height: 400px
	}*/
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?php echo lang('records'); ?></h1>
		<ol class="breadcrumb">
	        <li><a href="#">Home</a></li>
	        <li class="active"><?php echo lang('records'); ?></li>
      </ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-body">
				<!-- row -->
				<div class="row">
					<form action="<?php echo site_url('admin/cards/printCard')?>" method="post">
						<div class="col-xs-3 form-group">
							<label>card:</label>
						</div>
						<div class="col-xs-3 form-group">
							<select class="form-control" id="cards" name="card_id" onchange="preview_background()">
								<option value="">Select card</option>
							</select>
						</div>
						<div class="col-xs-3 form-group">
							<label>Employee No.:</label>
						</div>
						<div class="col-xs-3 form-group">
							<input type="text" name="employee_number" onchange="preview_detail()" onkeyup="preview_detail()" id="records" value="<?php echo $employee_number?>" class="form-control">
						</div>
						<div class="col-xs-3 form-group">
							<label>Id No.:</label>
						</div>
						<div class="col-xs-3 form-group">
							<input type="text" name="id_no" id="id_no" value="<?php echo $id_no?>" class="form-control">
							<!-- <select class="form-control" id="records" name="id" onchange="preview_detail()">
								<option value="">Select detail</option>
							</select> -->
<!-- <<<<<<< HEAD -->
						</div>
						<div class="col-xs-3 form-group">
							<label>Detail For Residence Card:</label>
						</div>
						<div class="col-xs-3 form-group">
							<input type="text" name="flat_no" id="flat_no" class="form-control">
						</div>
						<div class="col-xs-3 form-group">
							<label>Expiry Date:</label>
						</div>
						<div class="col-xs-3 form-group">
							<div name="expiry_date" id="expiry_date" class='date_box'></div>
						</div>
						<div class="col-xs-12 form-group">
<!-- ======= -->
						<!-- </div>
						<div class="col-xs-3 form-group"> -->
<!-- >>>>>>> fe7fc79dea564f511a3ef0f9ad823e457ad9b528 -->
							<button class="btn btn-primary">Submit</button>
						</div>
						<div id="background_preview" class="preview col-xs-6"></div>
						<div id="detail_record" class="preview col-xs-6"></div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script language="javascript" type="text/javascript">

$(function(){
	$.post('<?php echo site_url('admin/cards/combo_json')?>',null,function(rows) {
		$.each(rows,function (key,value) {
			$("#cards").append(new Option(value.card_type, value.id));
		})
	},'json');

	// $.post('<?php echo site_url('admin/records/combo_json')?>',null,function(rows) {
	// 	$.each(rows,function (key,value) {
	// 		$("#records").append(new Option(value.name, value.id));
	// 	})
	// },'json');
});
</script>

<script type="text/javascript">
	function preview_background() {
		var id = $('#cards').val();
		$.post('<?php echo site_url("admin/cards/get_card")?>',{id:id},function(result){
			var html_content = '<img src="<?php echo site_url().'/uploads/cards/';?>' + result.image + '" style="max-height:400px; max-width:500px; padding: 5px">';
			$('#background_preview').html(html_content);
		},'json');
	}
</script>

<script type="text/javascript">
	function preview_detail() {
		var id = $('#records').val();
		$.post('<?php echo site_url("admin/records/get_records")?>',{id:id},function(result){
			// var html_content = '<img src="<?php echo site_url().'/uploads/detail/';?>' + result.image + '" style="height:100px; max-width:100px; padding: 5px">';
			$('#id_no').val('');
			$('#detail_record').html(result);
		},'html');
	}
</script>