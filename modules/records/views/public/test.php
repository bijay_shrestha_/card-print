<link rel="stylesheet" href="<?php echo site_url()?>assets/css/image_crop/croppie.css" />
<script src="<?php echo site_url()?>assets/js/image_crop/croppie.js"></script>

<div class="demo"></div>
<script>
$('.demo').croppie({
    url: '<?php echo site_url()?>uploads/cards/card_20190214222532.jpg',
});
</script>
<!-- or even simpler -->
<img class="my-image" src="<?php echo site_url()?>uploads/test/card_20190224114249.png" style="height: 50px;"/>
<script language="javascript" type="text/javascript">
	$(function(){
		$('.my-image').croppie();
	});
</script>