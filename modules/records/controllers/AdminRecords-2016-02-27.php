<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------

/**
 * Records
 *
 * Extends the Project_Controller class
 * 
 */

class AdminRecords extends Project_Controller
{
	public function __construct()
	{
    	parent::__construct();

    	control('Records');

        $this->load->model('records/record_model');
        $this->lang->load('records/record');
    }

	public function index($record_type = NULL)
	{
		$data['has_upload'] = true;
		if($record_type){
			$data['record_type'] = $record_type;
		}else{
			$data['record_type'] = 'BE Staff';
		}
		// Display Page
		$data['header'] = lang('records');
		$data['page'] = $this->config->item('template_admin') . "index";
		$data['module'] = 'records';
		$this->load->view($this->_container,$data);
	}

	public function json($record_type = NULL)
	{
		search_params();
		$where = array();

		// if($record_type){
			$where['record_type'] = urldecode ($record_type);
		// }else{
		// 	$where['record_type'] = 'BE Staff';
		// }
		
		$total=$this->record_model->find_count($where);
		
		paging('id');
		
		search_params();
		
		$rows=$this->record_model->findAll($where);

		echo json_encode(array('total'=>$total,'rows'=>$rows));
		exit;
	}

	public function combo_json()
	{
		$rows=$this->record_model->findAll();
		echo json_encode($rows);
		exit;
	}

	public function get_records($id='')
	{
		$data['module'] = 'records';
		$id = $this->input->post('id');
		$data['result'] = $this->record_model->find(array('employee_number' => $id));
		if($data['result']){
			$this->load->view('admin/preview',$data);
		}
		// exit;
	}

	public function save()
	{
        $data=$this->_get_posted_data(); //Retrive Posted Data

        if(!$this->input->post('id'))
        {
            $success=$this->record_model->insert($data);
        }
        else
        {
            $success=$this->record_model->update($data['id'],$data);
        }

		if($success)
		{
			$success = TRUE;
			$msg=lang('general_success');
		}
		else
		{
			$success = FALSE;
			$msg=lang('general_failure');
		}

		 echo json_encode(array('msg'=>$msg,'success'=>$success));
		 exit;
	}

   	private function _get_posted_data()
   	{
   		$data=array();
   		if($this->input->post('id')) {
			$data['id'] = $this->input->post('id');
		}
		// $data['created_at'] = $this->input->post('created_at');
		// $data['created_by'] = $this->input->post('created_by');
		// $data['updated_at'] = $this->input->post('updated_at');
		// $data['updated_by'] = $this->input->post('updated_by');
		// $data['deleted_at'] = $this->input->post('deleted_at');
		// $data['deleted_by'] = $this->input->post('deleted_by');
		$data['name'] = $this->input->post('name');
		$data['id_no'] = $this->input->post('id_no');
		$data['employee_number'] = $this->input->post('employee_number');
		$data['image'] = $this->input->post('image');
		$data['blood_group'] = $this->input->post('blood_group');
		if($this->input->post('date_of_birth')){
			$data['date_of_birth'] = $this->input->post('date_of_birth');
		}
		$data['department'] = $this->input->post('department');
		$data['record_type'] = $this->input->post('record_type');

        return $data;
   	}

   	public function select_card($id_no = NULL,$employee_number = NULL)
   	{
   		$data['id_no'] = $id_no;
   		$data['employee_number'] = $employee_number;
   		// Display Page
		$data['header'] = lang('records');
		$data['page'] = $this->config->item('template_admin') . "select_card";
		$data['module'] = 'records';
		$this->load->view($this->_container,$data);
   	}

   	public function do_upload()
    {
    	// print_r($_FILES);
            $config['upload_path']          = './uploads/detail';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000000;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;
            $config['file_name']           	= 'card_' . date('YmdHis');

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload())
            {
                    $error = array('error' => $this->upload->display_errors());

                    echo json_encode(array('success' => false,'msg'=>$error,'file_name'=>''));

                    // print_r($error);
                    // $this->load->view('upload_form', $error);
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());

                    echo json_encode(array('success' => true, 'msg' => false , 'file_name' => $this->upload->data('file_name')));

                    // $this->load->view('upload_success', $data);
           }
   }

    public function test()
	{
		$data['has_upload'] = true;
		// Display Page
		$data['header'] = lang('records');
		$data['page'] = $this->config->item('template_admin') . "test";
		$data['module'] = 'records';
		$this->load->view($this->_container,$data);
	}

	public function testimg_save_to_file($value='')
	{
		$config['upload_path']          = './uploads/test';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1000000;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;
            $config['file_name']           	= 'card_' . date('YmdHis');

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('img'))
            {
                    $error = array('error' => $this->upload->display_errors());

                    echo json_encode(array('success' => false,'msg'=>$error,'file_name'=>''));

                    // print_r($error);
                    // $this->load->view('upload_form', $error);
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());

                    echo json_encode(array('status' => 'success', 'url' => $this->upload->data('full_path'),"width" => $this->upload->data('width'),
			"height" => $this->upload->data('height'), 'file_name' => $this->upload->data('file_name')));

                    // $this->load->view('upload_success', $data);
            }
		/*$imagePath = "/uploads/test";

		$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
		$temp = explode(".", $_FILES["img"]["name"]);
		$extension = end($temp);
		
		//Check write Access to Directory

		if(!is_writable($imagePath)){
			$response = Array(
				"status" => 'error',
				"message" => 'Can`t upload File; no write Access'
			);
			print json_encode($response);
			return;
		}
		
		if ( in_array($extension, $allowedExts))
		  {
		  if ($_FILES["img"]["error"] > 0)
			{
				 $response = array(
					"status" => 'error',
					"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
				);			
			}
		  else
			{
				
		      $filename = $_FILES["img"]["tmp_name"];
			  list($width, $height) = getimagesize( $filename );

			  move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

			  $response = array(
				"status" => 'success',
				"url" => $imagePath.$_FILES["img"]["name"],
				"width" => $width,
				"height" => $height
			  );
			  
			}
		  }
		else
		  {
		   $response = array(
				"status" => 'error',
				"message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
			);
		  }
		  
		  print json_encode($response);*/
	}

	public function delete($id='')
	{
		$success = $this->record_model->delete($id);

		echo json_encode(array('success' => $success));
	}

	public function index1($record_type = NULL)
	{
		$data['has_upload'] = true;
		if($record_type){
			$data['record_type'] = $record_type;
		}else{
			$data['record_type'] = 'BE Staff';
		}
		// Display Page
		$data['header'] = lang('records');
		$data['page'] = $this->config->item('template_admin') . "index_test";
		$data['module'] = 'records';
		$this->load->view($this->_container,$data);
	}

	public function do_upload1(){
// echo 'here';
		/*$image = $this->input->post('imgBase64');
		$fileName=$this->input->post('record_name').'.png';
		// var_dump($image);exit;
		$image = str_replace('data:image/png;base64,', '', $image);
		$image = str_replace(' ', '+', $image);
		$fileData = base64_decode($image);
		
		file_put_contents('./uploads/detail', $fileData);
		echo json_encode(array('image_name'=>$fileName,'success'=>$success));
		 exit;*/
		 
		 // echo"<pre>";
		// print_r($this->input->post());exit;
		define('UPLOAD_DIR', 'uploads/records/');
		$image = $this->input->post('imgBase64');
		$fileName=$this->input->post('record_name').'.png';
		
		$image = str_replace('data:image/png;base64,', '', $image);
		$image = str_replace(' ', '+', $image);
		$fileData = base64_decode($image);
		$file = UPLOAD_DIR . uniqid() . '.png';
		$success = file_put_contents($file, $fileData);
		
		echo json_encode($fileName);
		 exit;
	}

	public function excel_form()
	{
		// Display Page
		$data['header'] = lang('records');
		$data['page'] = $this->config->item('template_admin') . "excel_form";
		$data['module'] = 'records';
		$this->load->view($this->_container,$data);
	}

	public function excel_upload()
	{
		$index = array('id_no','employee_number','name','department','record_type','blood_group','date_of_birth');
		$data = $this->read_file('userfile','uploads/excels/',$index,array('date_of_birth'));

		print_r($data);

		foreach ($data as $key => $value) {
			$success = $this->record_model->insert($value);
		}

		redirect(site_url('admin/records'));
	}
}