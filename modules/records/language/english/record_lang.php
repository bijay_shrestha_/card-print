<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * PROJECT
 *
 * @package         PROJECT
 * @author          <AUTHOR_NAME>
 * @copyright       Copyright (c) 2016
 */

// ---------------------------------------------------------------------------


$lang['id'] = 'Id';
$lang['created_at'] = 'Created At';
$lang['created_by'] = 'Created By';
$lang['updated_at'] = 'Updated At';
$lang['updated_by'] = 'Updated By';
$lang['deleted_at'] = 'Deleted At';
$lang['deleted_by'] = 'Deleted By';
$lang['name'] = 'Name';
$lang['id_no'] = 'Id No';
$lang['employee_number'] = 'Employee Number';
$lang['image'] = 'Image';
$lang['blood_group'] = 'Blood Group';
$lang['date_of_birth'] = 'Date Of Birth';
$lang['department'] = 'Department';

$lang['records']='Records';
$lang['record_type'] = 'Record Type';